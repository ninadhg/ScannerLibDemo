package com.woluxi.scannerlib.data.preamble;

/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckBit7State implements State {
    PreambleStateMachine psm;

    public CheckBit7State(PreambleStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public String toString() {
        return "CheckBit7State{}";
    }

    @Override
    public void input(boolean inp) {
        if(inp){
            psm.setState(psm.getCheckBit0State());
        }else{
            psm.setState(psm.getCheckStopBitState());
        }
    }
}
