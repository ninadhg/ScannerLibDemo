package com.woluxi.scannerlib.data.packet;


/**
 * Created by Ninad on 9/18/2016.
 */
public class DataBitRecordState implements State {
    PacketStateMachine psm;
    int count;
    public DataBitRecordState(PacketStateMachine psm) {
        this.count = 0;
        this.psm = psm;
    }


    @Override
    public void input(boolean inp) {
        psm.recordBit(inp,count);
        if(this.count == 7) {
            psm.setState(psm.getCheckStopBitState());
            this.count = 0;
            return;
        }
        this.count++;
    }

    @Override
    public String toString() {
        return "DataBitRecordState{" +
                "count=" + count +
                '}';
    }
}
