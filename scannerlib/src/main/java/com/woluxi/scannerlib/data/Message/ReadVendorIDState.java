package com.woluxi.scannerlib.data.Message;

import com.woluxi.scannerlib.data.packet.PacketStateMachine;

/**
 * Created by Ninad on 9/19/2016.
 */
public class ReadVendorIDState implements State  {
    PacketStateMachine psm;
    MessageStateMachine msm;

    public ReadVendorIDState(MessageStateMachine msm) {
        this.msm = msm;
        psm = new PacketStateMachine();
    }

    @Override
    public void input(boolean inp) {
        psm.input(inp);
        if(psm.getBits().getErrorCount() > 2) {
            psm.reset();
            msm.reset();
            return;
        }
        if(psm.isPacketReadComplete()){
            System.out.println("vendor id read complete");
            msm.setVendorByte(psm.getBits());
            psm.reset();
            msm.setStateAfterVendorByteRead();
        }
    }

    @Override
    public String toString() {
        return "ReadVendorIDState{" +
                "psm=" + psm +
                '}';
    }
}
