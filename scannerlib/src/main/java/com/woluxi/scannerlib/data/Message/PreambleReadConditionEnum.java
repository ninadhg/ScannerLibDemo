package com.woluxi.scannerlib.data.Message;

/**
 * Created by ninadhg on 10/25/16.
 */

public enum PreambleReadConditionEnum {
    PREAMBLE_NOT_READ,
    PREAMBLE_READ,
    PREAMBLE_IN_METADATA,
    PREAMBLE_IN_VENDORID,
    PREAMBLE_IN_HWID,
    PREAMBLE_IN_DATA
}
