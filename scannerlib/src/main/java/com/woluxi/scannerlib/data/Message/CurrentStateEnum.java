package com.woluxi.scannerlib.data.Message;

/**
 * Created by ninadhg on 11/30/16.
 */

public enum CurrentStateEnum {
    INITIALIZED,
    PREAMBLE_COMPLETE,
    METADATA_COMPLETE,
    VENDOR_COMPLETE,
    HWMSGID_COMPLETE,
    DATA_FIRST_BYTE_READ,
    DATA_COMPLETE,
}
