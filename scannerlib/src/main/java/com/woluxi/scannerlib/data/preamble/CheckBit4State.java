package com.woluxi.scannerlib.data.preamble;


/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckBit4State implements State {
    PreambleStateMachine psm;

    public CheckBit4State(PreambleStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public String toString() {
        return "CheckBit4State{}";
    }

    @Override
    public void input(boolean inp) {
        if(inp){
            psm.setState(psm.getCheckBit4State());
        }else{
            psm.setState(psm.getCheckBit5State());
        }
    }
}
