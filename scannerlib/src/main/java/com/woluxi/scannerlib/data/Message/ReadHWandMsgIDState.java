package com.woluxi.scannerlib.data.Message;

import com.woluxi.scannerlib.data.packet.PacketStateMachine;

/**
 * Created by Ninad on 9/19/2016.
 */
public class ReadHWandMsgIDState implements State  {
    PacketStateMachine psm;
    MessageStateMachine msm;
    public ReadHWandMsgIDState(MessageStateMachine msm) {
        this.msm = msm;
        psm = new PacketStateMachine();
    }

    @Override
    public void input(boolean inp) {
        psm.input(inp);
        if(psm.getBits().getErrorCount() > 1) {
            psm.reset();
            msm.reset();
            return;
        }
        if(psm.isPacketReadComplete()){
            ByteRecord data;

            System.out.println("HW and MSG ID packet read complete");
            data = psm.getBits();
            msm.setHWIdandMsgId(data);
            psm.reset();
            msm.setStateAfterReadHWandMsgID();
        }
    }
    @Override
    public String toString() {
        return "ReadHWandMsgIDState{" +
                "psm=" + psm +
                '}';
    }
}
