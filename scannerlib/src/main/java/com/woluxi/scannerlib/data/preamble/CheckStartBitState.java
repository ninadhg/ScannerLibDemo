package com.woluxi.scannerlib.data.preamble;

/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckStartBitState implements State {
    PreambleStateMachine psm;

    public CheckStartBitState(PreambleStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public String toString() {
        return "CheckStartBitState{}";
    }

    @Override
    public void input(boolean inp) {
        if(inp){
            psm.setState(psm.getCheckBit0State());
        } // else remain in this state.
    }
}
