package com.woluxi.scannerlib.data.Message;

import com.woluxi.scannerlib.data.preamble.PreambleStateMachine;

/**
 * Created by Ninad on 9/19/2016.
 */
public class CheckPreambleState implements State {
    MessageStateMachine msm;
    PreambleStateMachine psm;

    public CheckPreambleState(MessageStateMachine msm) {
        psm = new PreambleStateMachine();
        this.msm = msm;
    }

    @Override
    public void input(boolean inp) {
        psm.input(inp);
        if (psm.isPreambleSatisfied()) {
            psm.reset();
            msm.setStateAfterPreambleRead();
        }
    }

    @Override
    public String toString() {
        return "CheckPreambleState{" +
                " psm=" + psm +
                '}';
    }
}
