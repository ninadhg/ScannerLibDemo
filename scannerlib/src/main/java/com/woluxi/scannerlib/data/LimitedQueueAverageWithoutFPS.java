package com.woluxi.scannerlib.data;

/**
 * Created by Ninad on 9/6/2016.
 */

import java.util.Iterator;


public class LimitedQueueAverageWithoutFPS extends LimitedQueueAverage {
    private int limit;
    private double avg;

    private static final String TAG = "CVDIAG:LimitedQueue";

    public LimitedQueueAverageWithoutFPS(int limit, double initdata) {
        this.limit = limit;
        add(initdata);
    }

    public void add(double o) {
        Double i = new Double(o);
        synchronized (this) {
            super.add(i);
            while (size() > limit) {
                super.remove();
            }
            avg = calculateAvg();
        }
        return;
    }

    @Override
    public double getFPS() {
        return 0;
    }

    public double getAvg() {
        return avg;
    }

    public double calculateAvg(){
        double count = 0;
        int num = 0;
        for (Iterator iterator = this.iterator(); iterator.hasNext(); ) {
            Double type = (Double) iterator.next();
            count = count + type;
            num = num + 1;
        }
        return count / (num);
    }}