package com.woluxi.scannerlib.data.preamble;

/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckBit5State implements State {
    PreambleStateMachine psm;

    @Override
    public String toString() {
        return "CheckBit5State{}";
    }

    public CheckBit5State(PreambleStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public void input(boolean inp) {
        if(inp){
            psm.setState(psm.getCheckBit0State());
        }else{
            psm.setState(psm.getCheckBit6State());
        }
    }
}
