package com.woluxi.scannerlib.data.preamble;

/**
 * Created by Ninad on 9/18/2016.
 */
public class PreambleStateMachine {
    @Override
    public String toString() {
        return "PreambleStateMachine{" +
                "state=" + state +
                ", preambleSatisfied=" + preambleSatisfied +
                '}';
    }

    public PreambleStateMachine() {
        this.checkStartBitState = new CheckStartBitState(this);
        this.checkStopBitState = new CheckStopBitState(this);
        this.preambleSatisfiedState = new PreambleSatisfiedState(this);
        this.checkBit0State = new CheckBit0State(this);
        this.checkBit1State = new CheckBit1State(this);
        this.checkBit2State = new CheckBit2State(this);
        this.checkBit3State = new CheckBit3State(this);
        this.checkBit4State = new CheckBit4State(this);
        this.checkBit5State = new CheckBit5State(this);
        this.checkBit6State = new CheckBit6State(this);
        this.checkBit7State = new CheckBit7State(this);
        this.state = checkStartBitState;
    }

    public boolean isPreambleSatisfied() {
        return preambleSatisfied;
    }

    public void setPreambleSatisfied(boolean preambleSatisfied) {
        this.preambleSatisfied = preambleSatisfied;
    }

    boolean preambleSatisfied = false;

    State checkStartBitState;
    State checkStopBitState;
    State preambleSatisfiedState;

    State checkBit0State;
    State checkBit1State;
    State checkBit2State;
    State checkBit3State;
    State checkBit4State;
    State checkBit5State;
    State checkBit6State;
    State checkBit7State;

    State state = checkStartBitState;

    public State getCheckStartBitState() {
        return checkStartBitState;
    }

    public State getPreambleSatisfiedState() {
        return preambleSatisfiedState;
    }

    public State getCheckStopBitState() {
        return checkStopBitState;
    }

    public State getCheckBit7State() {
        return checkBit7State;
    }

    public State getCheckBit6State() {
        return checkBit6State;
    }

    public State getCheckBit5State() {
        return checkBit5State;
    }

    public State getCheckBit4State() {
        return checkBit4State;
    }

    public State getCheckBit3State() {
        return checkBit3State;
    }

    public State getCheckBit2State() {
        return checkBit2State;
    }

    public State getCheckBit1State() {
        return checkBit1State;
    }

    public State getCheckBit0State() {
        return checkBit0State;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void input(boolean inp) {
        //System.out.println("current state is " + state);
        state.input(inp);
    }

    public void resetStartBitRead() {
        this.state = checkBit0State;
        preambleSatisfied = false;
    }
    public void reset(){
        this.state = checkStartBitState;
        preambleSatisfied = false;
    }

}
