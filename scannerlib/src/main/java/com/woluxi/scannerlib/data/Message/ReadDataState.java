package com.woluxi.scannerlib.data.Message;

import com.woluxi.scannerlib.data.packet.PacketStateMachine;

/**
 * Created by Ninad on 9/19/2016.
 */
public class ReadDataState implements State  {
    PacketStateMachine psm;
    MessageStateMachine msm;

    public ReadDataState(MessageStateMachine msm) {
        this.msm = msm;
        psm = new PacketStateMachine();
    }

    @Override
    public void input(boolean inp) {
        psm.input(inp);
        if(psm.getBits().getErrorCount() > 1) {
            psm.reset();
            msm.reset();
            return;
        }
        if(psm.isPacketReadComplete()){
            System.out.println("packet read complete");
            msm.setDataByte(psm.getBits());
            psm.reset();
            msm.setStateAfterDataRead();
        }
    }

    @Override
    public String toString() {
        return "ReadDataState{" +
                "psm=" + psm +
                '}';
    }
}
