package com.woluxi.scannerlib.data.preamble;

/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckBit2State implements State {
    PreambleStateMachine psm;

    public CheckBit2State(PreambleStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public String toString() {
        return "CheckBit2State{}";
    }

    @Override
    public void input(boolean inp) {
        if(inp){
            psm.setState(psm.getCheckBit3State());
        }else{
            psm.setState(psm.getCheckStartBitState());
        }
    }
}
