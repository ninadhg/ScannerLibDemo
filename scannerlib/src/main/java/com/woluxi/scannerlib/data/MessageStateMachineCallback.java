package com.woluxi.scannerlib.data;

import com.woluxi.scannerlib.data.Message.MessageRecord;

/**
 * Created by ninadhg on 11/30/16.
 */

public interface MessageStateMachineCallback {
    void preambleReadComplete();
    void metaDataReadComplete();
    void vendorReadComplete();
    void hwMsgIdReadComplete();
    void dataReadComplete(MessageRecord msg);
    void initComplete();
    void dataFirstByteRead();
}
