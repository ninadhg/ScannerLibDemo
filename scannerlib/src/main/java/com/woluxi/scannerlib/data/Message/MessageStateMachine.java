package com.woluxi.scannerlib.data.Message;

import android.util.Log;

import com.woluxi.scannerlib.data.confidence.MessageStoreAndCorrector;

public enum  MessageStateMachine {
    INSTANCE;

    private State checkPreambleState = new CheckPreambleState(this);
    private State readMetaDataState = new ReadMetaDataState(this);
    private State readVendorIDState = new ReadVendorIDState(this);
    private State readHWandMsgIDState = new ReadHWandMsgIDState(this);
    private State readDataState = new ReadDataState(this);
    CurrentStateEnum currentStateEnum = CurrentStateEnum.INITIALIZED;

    private boolean idOptional = false;

    public void setMetaDataOptional(boolean metaDataOptional) {
        this.metaDataOptional = metaDataOptional;
    }

    private boolean metaDataOptional = false;

    /**
     *  If this parameter is set the both the vendorId and Hw Id is treated as optional.
     * @param idOptional if vendorId and hwId are present in the message.
     */
    public void setIdOptional(boolean idOptional) {
        this.idOptional = idOptional;
    }


    public CurrentStateEnum getState() {
        return currentStateEnum;
    }

    private State state = checkPreambleState;

    private static final String TAG = "MessageStateMachine";

    public PreambleReadConditionEnum isPreambleSatisfied() {
        return preambleCondition;
    }

    private PreambleReadConditionEnum preambleCondition = PreambleReadConditionEnum.PREAMBLE_NOT_READ;
    MessageRecord msgRecord;

    private int curDataPos = 0;
    private int dataLen;
    private int curVendorPos = 0 ;
    private int mode = 0;

    public void reset() {
        state = checkPreambleState;
        curDataPos = 0;
        curVendorPos = 0;
        preambleCondition = PreambleReadConditionEnum.PREAMBLE_NOT_READ;
        currentStateEnum = CurrentStateEnum.INITIALIZED;
        preambleDataFound = false;
    }

    public void setStateAfterPreambleRead() {
        preambleCondition = PreambleReadConditionEnum.PREAMBLE_READ;
        currentStateEnum = CurrentStateEnum.PREAMBLE_COMPLETE;
        if(metaDataOptional) {
            state = readDataState;
            dataLen = 1;
            msgRecord = new MessageRecord();
            msgRecord.setMetaData(new ByteRecord(0x09)); //XXX fix this..
        } else
            state = readMetaDataState;
    }

    public void setStateAfterMetaDataRead() {
        if(msgRecord.getMetaData().getErrorCount() > 1) {
            reset();
            return;
        }
        if(preambleCondition != PreambleReadConditionEnum.PREAMBLE_READ) {
            preambleCondition = PreambleReadConditionEnum.PREAMBLE_READ;
            this.state = readMetaDataState;
            currentStateEnum = CurrentStateEnum.PREAMBLE_COMPLETE;
        } else {
            if(this.mode == 0x02) {
                this.state = readDataState;
            } else {
                if(this.idOptional)
                    this.state = readDataState;
                else
                    this.state = readVendorIDState;
            }
            currentStateEnum = CurrentStateEnum.METADATA_COMPLETE;
        }
    }

    public void setStateAfterVendorByteRead() {
        if(preambleCondition != PreambleReadConditionEnum.PREAMBLE_READ) {
            preambleCondition = PreambleReadConditionEnum.PREAMBLE_READ;
            currentStateEnum = CurrentStateEnum.PREAMBLE_COMPLETE;
            this.state = readMetaDataState;
        } else {
            if (curVendorPos == 2) {
                state = readHWandMsgIDState;
                currentStateEnum = CurrentStateEnum.VENDOR_COMPLETE;

            } //else remain in same state.
        }
    }

    public void setStateAfterReadHWandMsgID() {
        if(preambleCondition != PreambleReadConditionEnum.PREAMBLE_READ) {
            preambleCondition = PreambleReadConditionEnum.PREAMBLE_READ;
            currentStateEnum = CurrentStateEnum.PREAMBLE_COMPLETE;
            this.state = readMetaDataState;
        } else {
            state = readDataState;
            currentStateEnum = CurrentStateEnum.HWMSGID_COMPLETE;

        }
    }

    public void setStateAfterDataRead() {
        //todo: if the metadata was corrupted then we can get more bytes
        //Log.d(TAG, " curDataPos " + curDataPos + " dataLen " + dataLen);
        if(preambleCondition == PreambleReadConditionEnum.PREAMBLE_IN_DATA) {
            currentStateEnum = CurrentStateEnum.PREAMBLE_COMPLETE;
            this.state = readMetaDataState;
            curDataPos = 0;
        }
        if(curDataPos >= dataLen) {
            MessageStoreAndCorrector.INSTANCE.addRecord(msgRecord);
            currentStateEnum = CurrentStateEnum.DATA_COMPLETE;
        } else {
            currentStateEnum = CurrentStateEnum.DATA_FIRST_BYTE_READ;
        }
    }

    public void input(boolean inp) {
        //Log.d(TAG,"input " + inp + " cur state " + state);
        state.input(inp);
    }

    private boolean preambleDataFound = false;

    void setDataByte(ByteRecord data){
        byte dt = data.getByte();
        //Log.e(TAG, "setting data byte  " + Integer.toHexString(dt) + " at position " + curDataPos);
        if(dt == (byte)0xf0) {
            if(preambleDataFound) {
                preambleDataFound = false;
                curDataPos++;
                msgRecord.addDataByte(data);
            } else {
                preambleDataFound = true;
            }
            return;
        }
        if (preambleDataFound) {
            Log.e(TAG, "preceeding preamble found, probably a bad Message");
            preambleCondition = PreambleReadConditionEnum.PREAMBLE_IN_DATA;
            return;
        }
        curDataPos++;
        msgRecord.addDataByte(data);
    }

    void setMetaData(ByteRecord inp){
        byte data = inp.getByte();
        Log.d(TAG, "MetaData " + String.format("%02X",data));
        if(data == (byte)0xf0) {
            preambleCondition = PreambleReadConditionEnum.PREAMBLE_IN_METADATA;
        }

        this.dataLen = (data & 0x07 )+ 1;
        this.mode = (data & 0x30) >> 4;
        Log.d(TAG, "Metadata mode is " + this.mode);
        msgRecord = new MessageRecord();
        msgRecord.setMetaData(inp);

    }

    public void setVendorByte(ByteRecord data) {
        Log.d(TAG, "VendorByte  " + String.format("%02X",data.toByteArray()[0]));

        if(data.toByteArray()[0] == (byte)0xf0) {
            preambleCondition = PreambleReadConditionEnum.PREAMBLE_IN_VENDORID;
        }
        msgRecord.setVendorId(data,curVendorPos++);
    }

    public void setHWIdandMsgId(ByteRecord data) {
        Log.d(TAG, "HWIdandMsgId " + String.format("%02X",data.toByteArray()[0]));

        if(data.toByteArray()[0] == (byte)0xf0) {
            preambleCondition = PreambleReadConditionEnum.PREAMBLE_IN_HWID;
        }
        msgRecord.setHwAndMsgId(data);

    }

    public byte [] getData(){
        if(msgRecord == null)
            return null;
        else
            return msgRecord.getDataAsBytes();
    }

    public String getInfo() {
        if(msgRecord == null)
            return null;
        else
            return msgRecord.getInfo();
    }

    public MessageRecord getRecord() {
        return msgRecord;
    }
    public int getMsgId(){
        return msgRecord.getMessageId();
    }
    @Override
    public String toString() {
        return "MessageStateMachine{" +
                "state=" + state +
                ", msgRecord=" + msgRecord +
                ", curDataPos=" + curDataPos +
                '}';
    }
}
