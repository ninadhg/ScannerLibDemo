package com.woluxi.scannerlib.data.preamble;

/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckBit6State implements State {
    PreambleStateMachine psm;

    @Override
    public String toString() {
        return "CheckBit6State{}";
    }

    public CheckBit6State(PreambleStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public void input(boolean inp) {
        if(inp){
            psm.setState(psm.getCheckBit0State());
        }else{
            psm.setState(psm.getCheckBit7State());
        }
    }
}
