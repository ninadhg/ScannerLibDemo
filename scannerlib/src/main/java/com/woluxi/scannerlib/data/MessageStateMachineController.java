package com.woluxi.scannerlib.data;

import android.util.Log;

import com.woluxi.scannerlib.data.Message.CurrentStateEnum;
import com.woluxi.scannerlib.data.Message.MessageStateMachine;


/**
 * Created by ninadhg on 11/29/16.
 */

public enum MessageStateMachineController {
    INSTANCE;
    private static final String TAG = "MsgStMchnCtrl";

    private MessageStateMachine msg = MessageStateMachine.INSTANCE;
    private volatile boolean lastLedStatus = false;
    private volatile long lastTimeStamp = System.currentTimeMillis();
    private static final long TIMESLICE = 250;
    private long bitTimeSlice = TIMESLICE;
    CurrentStateEnum previous = CurrentStateEnum.INITIALIZED;
    int currentMsgId = 0;
    private MessageStateMachineCallback callback;

    public void init(int bitTimeSlice){
        this.bitTimeSlice = bitTimeSlice;
    }

    public synchronized void feedMessageStateMachine(boolean led) {
        long currentTime = System.currentTimeMillis();

        if(led != lastLedStatus || (currentTime > lastTimeStamp + (bitTimeSlice * 8))) {
            // calculate the time slices
            long diffTime = currentTime - lastTimeStamp;
            int slices = (int) Math.round((double)diffTime/bitTimeSlice);
            long leftover = diffTime % bitTimeSlice;
            //Log.d(TAG,"found out we have to send " + slices + " state " + lastLedStatus + " " + lastTimeStamp + " " + currentTime);
            //Log.d(TAG, "leftover " + leftover);
            for (int i = 0; i < slices; i++) {
                msg.input(lastLedStatus);
                doCallback();
            }
            lastTimeStamp = currentTime;
            lastLedStatus = led;
        }
    }
    static int withoutCallback = 9;

    void doCallback(){
        //todo: Only one callback this should be a publisher subscriber pattern.
        if(callback != null) {
            CurrentStateEnum cur = msg.getState();
            //Log.d(TAG,"current state" + cur + " previous state " + previous + " callback count" + withoutCallback);
            if ( ++withoutCallback > 10 || previous != cur) {
                withoutCallback = 0;
                switch (cur) {
                    case INITIALIZED:
                        callback.initComplete();
                        break;
                    case PREAMBLE_COMPLETE:
                        callback.preambleReadComplete();
                        break;
                    case METADATA_COMPLETE:
                        callback.metaDataReadComplete();
                        break;
                    case VENDOR_COMPLETE:
                        callback.vendorReadComplete();
                        break;
                    case HWMSGID_COMPLETE:
                        callback.hwMsgIdReadComplete();
                        currentMsgId = msg.getMsgId();
                        break;
                    case DATA_FIRST_BYTE_READ:
                        callback.dataFirstByteRead();
                        break;
                    case DATA_COMPLETE:
                        callback.dataReadComplete(msg.getRecord());
                        msg.reset();
                        //Log.d(TAG,"Data read complete");
                        break;
                }
            }
            previous = cur;
        }
    }

    public int getCurrentMsgId(){
        return currentMsgId;
    }
    public void registerCallback(MessageStateMachineCallback callback){
        this.callback = callback;
    }

    public void unregisterCallback(){
        this.callback = null;
    }

}
