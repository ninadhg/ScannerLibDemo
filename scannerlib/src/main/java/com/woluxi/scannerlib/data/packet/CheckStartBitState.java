package com.woluxi.scannerlib.data.packet;


/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckStartBitState implements State {
    PacketStateMachine psm;

    public CheckStartBitState(PacketStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public String toString() {
        return "CheckStartBitState{}";
    }

    @Override
    public void input(boolean inp) {
        if (inp) {
            psm.setState(psm.getDataBitRecordState());
        } else { //else remain in this state, but most of the packet would be garbage after this.
            psm.setPacketAsError();
        }
    }
}
