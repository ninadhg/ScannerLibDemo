package com.woluxi.scannerlib.data.packet;



/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckStopBitState implements State {
    PacketStateMachine psm;

    public CheckStopBitState(PacketStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public void input(boolean inp) {
        if(inp){
            psm.setPacketAsError();
        } else {
            psm.setPacketReadComplete(true);
        }
        psm.setState(psm.getCheckStartBitState());
    }

    @Override
    public String toString() {
        return "CheckStopBitState{}";
    }
}
