package com.woluxi.scannerlib.data.preamble;

/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckStopBitState implements State {
    PreambleStateMachine psm;

    public CheckStopBitState(PreambleStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public void input(boolean inp) {
        if(inp){
            psm.setState(psm.getCheckBit0State());
        }else{
            psm.setPreambleSatisfied(true);
        }
    }

    @Override
    public String toString() {
        return "CheckStopBitState{}";
    }
}
