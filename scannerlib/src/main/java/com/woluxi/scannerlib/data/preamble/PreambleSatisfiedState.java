package com.woluxi.scannerlib.data.preamble;

/**
 * Created by Ninad on 9/18/2016.
 */
public class PreambleSatisfiedState implements State {
    PreambleStateMachine psm;

    public PreambleSatisfiedState(PreambleStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public void input(boolean inp) {
        return;
    }

    @Override
    public String toString() {
        return "PreambleSatisfiedState{}";
    }
}
