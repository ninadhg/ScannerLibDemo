package com.woluxi.scannerlib.data.packet;

/**
 * Created by Ninad on 9/18/2016.
 */
public interface State {
    public void input(boolean inp);
}
