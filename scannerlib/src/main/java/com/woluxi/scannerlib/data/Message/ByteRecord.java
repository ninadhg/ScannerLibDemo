package com.woluxi.scannerlib.data.Message;

import android.util.Log;

import java.util.BitSet;

/**
 * Created by ninadhg on 11/14/16.
 */

public class ByteRecord extends BitSet {
    private static final String TAG = "ByteRecord";

    public int getErrorCount() {
        return errorCount;
    }

    public void incrementErrorCount() {
        this.errorCount++;
        Log.d(TAG ," current ErrorCount is " + errorCount);
    }

    int errorCount;

    public ByteRecord(int i){
        super(i);
    }

    public byte getByte(){
        if(this.toByteArray() == null || this.toByteArray().length == 0)
            return 0x0;
        else
            return this.toByteArray()[0];
    }

    @Override
    public byte [] toByteArray() {
        if(this.isEmpty()){
            return new byte[]{0,0,0,0,0,0,0,0};
        } else {
            return super.toByteArray();
        }
    }

    @Override
    public String toString() {
        return "ByteRecord{" +
                "BitSet=" + super.toString() +
                "errorCount=" + errorCount +
                '}';
    }

}
