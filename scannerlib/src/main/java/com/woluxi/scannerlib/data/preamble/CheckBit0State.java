package com.woluxi.scannerlib.data.preamble;

/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckBit0State implements State {
    PreambleStateMachine psm;

    public CheckBit0State(PreambleStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public void input(boolean inp) {
        if (inp) {
            psm.setState(psm.getCheckBit1State());
        } else {
            psm.setState(psm.getCheckStartBitState());
        }
    }

    @Override
    public String toString() {
        return "CheckBit0State{}";
    }
}
