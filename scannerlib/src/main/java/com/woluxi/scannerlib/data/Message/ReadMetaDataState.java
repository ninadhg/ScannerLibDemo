package com.woluxi.scannerlib.data.Message;

import com.woluxi.scannerlib.data.packet.PacketStateMachine;

/**
 * Created by Ninad on 9/19/2016.
 */
public class ReadMetaDataState implements State  {
    PacketStateMachine psm;
    MessageStateMachine msm;

    public ReadMetaDataState(MessageStateMachine msm) {
        this.msm = msm;
        psm = new PacketStateMachine();
    }

    @Override
    public void input(boolean inp) {
        psm.input(inp);
        if(psm.getBits().getErrorCount() > 1) {
            psm.reset();
            msm.reset();
        }
        if(psm.isPacketReadComplete()){
            ByteRecord data;
            data = psm.getBits();
            msm.setMetaData(data);
            msm.setStateAfterMetaDataRead();
            psm.reset();
        }
    }

    @Override
    public String toString() {
        return "ReadMetaDataState{" +
                "psm=" + psm +
                '}';
    }
}
