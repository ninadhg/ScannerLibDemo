package com.woluxi.scannerlib.data.preamble;


/**
 * Created by Ninad on 9/18/2016.
 */
public class CheckBit3State implements State {
    PreambleStateMachine psm;

    @Override
    public String toString() {
        return "CheckBit3State{}";
    }

    public CheckBit3State(PreambleStateMachine psm) {
        this.psm = psm;
    }

    @Override
    public void input(boolean inp) {
        if(inp){
            psm.setState(psm.getCheckBit4State());
        }else{
            psm.setState(psm.getCheckStartBitState());
        }
    }
}
