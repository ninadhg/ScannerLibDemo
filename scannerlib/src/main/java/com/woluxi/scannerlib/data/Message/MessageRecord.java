package com.woluxi.scannerlib.data.Message;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ninadhg on 11/8/16.
 */

public class MessageRecord {
    private List<ByteRecord> dataBytes ;

    private ByteRecord metaData;
    private ByteRecord[] vendorId;
    private ByteRecord hwAndMsgId;

    private int mode;



    private int formatOrUnusedBit;
    private int version;
    private int dataLen;
    private byte hardwareID;
    private byte messageId;

    public void incrementErrorCount(){
        synchronized (this) {
            errorCount++;
        }
    }

    public int getErrorCount() {
        synchronized (this) {
            return errorCount;
        }
    }

    private int errorCount;

    private static final String TAG = "MessageRecord";

    public MessageRecord() {
        errorCount = 0;
    }

    public int getMode() {
        synchronized (this) {
            return mode;
        }
    }

    public int getVersion() {
        synchronized (this) {
            return version;
        }
    }

    public int getFormatOrUnusedBit() {
        return formatOrUnusedBit;
    }

    public void setFormatOrUnusedBit(int formatOrUnusedBit) {
        this.formatOrUnusedBit = formatOrUnusedBit;
    }

    public List<ByteRecord> getDataBytes() {
        synchronized (this) {
            return dataBytes;
        }
    }

    public void setDataBytes(List<ByteRecord> dataBytes) {
        synchronized (this) {
            this.dataBytes = dataBytes;
        }
    }

    public byte[] getDataAsBytes() {
        synchronized (this) {
            byte[] byteArray = new byte[dataBytes.size()];
            for (int i = 0; i < dataBytes.size(); i++) {
                byteArray[i] = dataBytes.get(i).toByteArray()[0];
            }
            return byteArray;
        }
    }

    public void addDataByte(ByteRecord data){
        synchronized (this) {
            dataBytes.add(data);
        }
    }

    public ByteRecord getHwAndMsgId() {
        synchronized (this) {
            return hwAndMsgId;
        }
    }

    public void setHwAndMsgId(ByteRecord inp) {
        byte data = inp.getByte();
        synchronized (this) {
            this.hwAndMsgId = inp;
            this.messageId = (byte) (data & 0x07);
            this.hardwareID = (byte) ((data & 0xF8) >> 3);
        }
    }

    public byte getHwid() {
        synchronized (this) {
            return hardwareID;
        }
    }

    public byte getMessageId()
    {
        synchronized (this) {
            return messageId;
        }
    }

    public void setMessageId(byte messageId) {
        synchronized (this) {
            this.messageId = messageId;
        }
    }

    public ByteRecord getMetaData() {
        synchronized (this) {
            return metaData;
        }
    }

    public void setMetaData(ByteRecord metaData) {
        synchronized (this) {
            this.metaData = metaData;
            byte data = metaData.getByte();
            this.version = (data & 0xA0) >> 6;
            if (this.version != 0x0) {
                Log.d(TAG, "Unknown version string" + this.version);
            }
            this.mode = (data & 0x30) >> 4;
            this.formatOrUnusedBit = (data & 0x08) >> 3;
            this.dataLen = data & 0x07;
            dataBytes = new ArrayList<ByteRecord>();
            vendorId = new ByteRecord[2];
        }
    }

    public ByteRecord[] getVendorId() {
        synchronized (this) {
            return vendorId;
        }
    }

    public int getVendorIdAsInt() {
        synchronized (this) {
            return (vendorId[0].getByte() << 8) | vendorId[1].getByte();
        }
    }

    public void setVendorId(ByteRecord vendor, int pos) {
        synchronized (this) {
            this.vendorId[pos] = vendor;
        }
    }

    public int getDataLen() {
        synchronized (this) {
            return dataLen;
        }
    }
    public String getInfo() {
        byte vendor0 = 0x0;
        byte vendor1 = 0x0;
        synchronized (this) {
            Log.d(TAG, "for getInfo " + this);
            if (vendorId[0] != null && vendorId[1] != null) {
                vendor0 = vendorId[0].toByteArray()[0];
                vendor1 = vendorId[1].toByteArray()[0];
            }
            return String.format("%02X:%02X:%02X%02X", metaData.toByteArray()[0], hardwareID, vendor0, vendor1);
        }
    }

    @Override
    public String toString() {
        synchronized (this) {
            return "MessageRecord{" +
                    "dataBytes=" + dataBytes +
                    ", hardwareID=" + hardwareID +
                    ", messageId=" + messageId +
                    ", metaData=" + metaData +
                    ", vendorId=" + Arrays.toString(vendorId) +
                    ", mode=" + mode +
                    ", version=" + version +
                    ", dataLen=" + dataLen +
                    '}';
        }
    }

    public boolean isEqual(MessageRecord rec) {
        synchronized (this) {
            if (!metaData.equals(rec.metaData))
                return false;
            if (!vendorId[0].equals(rec.vendorId[0]))
                return false;
            if (!vendorId[1].equals(rec.vendorId[1]))
                return false;
            if (!hwAndMsgId.equals(rec.hwAndMsgId))
                return false;

            for (int i = 0; i < dataBytes.size(); i++) {
                if (!dataBytes.get(i).equals(rec.dataBytes.get(i)))
                    return false;
            }
            return true;
        }
    }
}
