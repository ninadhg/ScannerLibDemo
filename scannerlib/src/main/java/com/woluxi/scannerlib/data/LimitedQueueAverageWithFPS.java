package com.woluxi.scannerlib.data;

/**
 * Created by Ninad on 9/6/2016.
 */

import java.util.Iterator;


public class LimitedQueueAverageWithFPS extends LimitedQueueAverage {
    private int limit;
    private double avg;
    private static final String TAG = "Woluxi:LimitedQueue";
    static int counter;

    private class TimeAndAvg{
        double avg;

        public long getTimestamp() {
            return timestamp;
        }

        long timestamp;

        public TimeAndAvg(double avg, long timestamp) {
            this.avg = avg;
            this.timestamp = timestamp;
        }
    }

    public LimitedQueueAverageWithFPS(int limit, double initdata) {
        this.limit = limit;
        super.add(new TimeAndAvg(initdata, System.currentTimeMillis()));
    }

    public void add(double o){
        long t = System.currentTimeMillis();
        //Log.d(TAG,"adding " + o + " t " + t);
        if( (counter++ % 3) != 0)
            return;

        TimeAndAvg ta = new TimeAndAvg(o, t);
        synchronized (this) {
            super.add(ta);
            while (size() > limit) {
                super.remove();
            }
            avg = calculateAvg();
        }
        return;
    }

    public double getFPS() {
        synchronized (this){
            TimeAndAvg l = (TimeAndAvg)this.getLast();
            TimeAndAvg f = (TimeAndAvg)this.getFirst();

            double diff = l.getTimestamp() - f.getTimestamp();
            // 3 because we do a % 3 for getting avgs in MessageStateMachineController
            // only sample 1 of 3 frames for dynamic avg
            double fps =  3 * (size() / diff) * 1000;
            //Log.d(TAG, " current fps " + fps + " diff "+ diff);
            return fps;
        }
    }

    public double getAvg() {
        return avg;
    }

    public double calculateAvg(){
        double count = 0;
        int num = 0;
        for (Iterator iterator = this.iterator(); iterator.hasNext(); ) {
            TimeAndAvg ta = (TimeAndAvg) iterator.next();
            count = count + ta.avg;
            num = num + 1;
        }
        return count / (num);
    }
}