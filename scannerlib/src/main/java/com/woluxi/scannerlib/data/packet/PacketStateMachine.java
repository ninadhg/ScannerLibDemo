package com.woluxi.scannerlib.data.packet;

import com.woluxi.scannerlib.data.Message.ByteRecord;

/**
 * Created by Ninad on 9/19/2016.
 */
public class PacketStateMachine {
    State state;
    State checkStartBitState;
    State checkStopBitState;

    public State getDataBitRecordState() {
        return dataBitRecordState;
    }

    State dataBitRecordState;
    boolean packetReadComplete;

    public ByteRecord getBits() {
        return bits;
    }

    ByteRecord bits;

    public boolean isPacketReadComplete() {
        return packetReadComplete;
    }

    public void setPacketReadComplete(boolean packetReadComplete) {
        this.packetReadComplete = packetReadComplete;
    }

    public State getCheckStopBitState() {
        return checkStopBitState;
    }

    public State getCheckStartBitState() {
        return checkStartBitState;
    }

    public PacketStateMachine(){
        this.checkStartBitState = new CheckStartBitState(this);
        this.checkStopBitState = new CheckStopBitState(this);
        dataBitRecordState = new DataBitRecordState(this);
        bits = new ByteRecord(8);
        state = checkStartBitState;
    }

    public void recordBit(boolean inp,int position){
        //System.out.println("setting " + position + " " + inp);
        bits.set(7-position,inp);
    }

    public void setPacketAsError() {
        bits.incrementErrorCount();
    }

    public void reset(){
        this.state = checkStartBitState;
        packetReadComplete =false;
        bits = new ByteRecord(8);
    }

    public void setState(State state) {
        this.state = state;
    }

    public void input(boolean inp) {
        state.input(inp);
    }

    @Override
    public String toString() {
        return "PacketStateMachine{" +
                "state=" + state +
                ", packetReadComplete=" + packetReadComplete +
                ", bits=" + bits +
                '}';
    }
}
