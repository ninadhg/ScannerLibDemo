package com.woluxi.scannerlib.data.confidence;

import android.util.Log;

import com.woluxi.scannerlib.data.Message.ByteRecord;
import com.woluxi.scannerlib.data.Message.MessageRecord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


/**
 * Created by ninadhg on 11/7/16.
 */

public enum MessageStoreAndCorrector {
    INSTANCE;

    private static final String TAG = "MesgStoreAndCorrector";

    private List<List<MessageRecord>> records ;
    private  MessageRecord[]  finalRecords;
    private Queue<MessageRecord> toBeProcessed ;

    private MessageStoreAndCorrector() {
        System.out.println("initializing");
        reset();
    }


    public void addRecord(MessageRecord record) {
        toBeProcessed.add(record);
    }

    private synchronized void processRecords(){
        System.out.println("process Records " + toBeProcessed.size());
        //Nothing to do.
        if(toBeProcessed.isEmpty())
            return;

        while(toBeProcessed.size() > 0){
            MessageRecord rec;

            rec = toBeProcessed.remove();
            int msgId = rec.getMessageId();

            System.out.println("yuk process Record " + rec + " size " + records.get(msgId).size());

            MessageRecord fin = finalRecords[msgId];
            //This is probably the first entry.
            if(fin == null) {
                Log.d(TAG, "No entry found in final" + rec);
                //Since this is the first msgID encountered we have to check if there is a corruption in the hwandmsgid byte.
                if(!checkMsgIdCorrupted(rec)) {
                    System.out.println("records " + records + "msg id " + msgId + " to be processed "+ toBeProcessed);
                    List<MessageRecord> msgRec = records.get(msgId);
                    msgRec.add(rec);
                    finalRecords[msgId] = rec;
                } else {
                    //todo: do something about it.
                    Log.e(TAG, "recceived Message has a similarity with other messages");
                    List<MessageRecord> msgRec = records.get(msgId);
                    msgRec.add(rec);
                    finalRecords[msgId] = rec;
                }
                continue;
            }
            records.get(msgId).add(rec);

            //If the recorded Message is equal to  the one in final common case.
            if( !fin.isEqual(rec)){
                //now parse through the list to determine the winner.
                determineFinalRecord(msgId, rec);
            }
        }
    }

    private int getCardinalityDiff(ByteRecord fMD, ByteRecord rMD){
        fMD = (ByteRecord) fMD.clone();
        fMD.xor(rMD);
        return fMD.cardinality();
    }

    private boolean checkMsgIdCorrupted(MessageRecord rec) {
        // go through the final list to see if there is anyother Message like this.
        for (int i = 0; i < finalRecords.length; i++) {
            MessageRecord mr = finalRecords[i];
            if(mr != null){
                //check metadata
                //if big difference then treat this as a different Message.
                if(getCardinalityDiff(mr.getMetaData(), rec.getMetaData() ) > 2)
                    return false;
                //check vendorid
                if(getCardinalityDiff(mr.getVendorId()[0], rec.getVendorId()[0] ) > 2)
                    return false;
                if(getCardinalityDiff(mr.getVendorId()[1], rec.getVendorId()[1] ) > 2)
                    return false;
                //check msgandhwid
                if(getCardinalityDiff(mr.getHwAndMsgId(), rec.getHwAndMsgId() ) > 2)
                    return false;
                // todo: checking data doesnt make sense because same data can be present.
            }else {
               continue;
            }
        }
        return false;
    }

    private void determineFinalRecord(int msgId, MessageRecord rec) {
        List<ByteRecord> brMetaData = new ArrayList<ByteRecord>();
        List<ByteRecord> brVendor0 = new ArrayList<ByteRecord>();
        List<ByteRecord> brVendor1 = new ArrayList<ByteRecord>();
        List<ByteRecord> brHwMsgId = new ArrayList<ByteRecord>();

        List<List<ByteRecord>> data = new ArrayList<List<ByteRecord>>();
        System.out.println("record size " + records.get(msgId).size());
        for (int i = 0; i < records.get(msgId).size(); i++) {
            MessageRecord msg = records.get(msgId).get(i);
            brMetaData.add(msg.getMetaData());
            brVendor0.add(msg.getVendorId()[0]);
            brVendor1.add(msg.getVendorId()[1]);
            brHwMsgId.add(msg.getHwAndMsgId());
            data.add(msg.getDataBytes());
            //Data can their lengths be different?!!!
        }
        System.out.println("before metadata" + brMetaData);
        ByteRecord finMetaData = getSanitizedByteRecord(brMetaData);
        System.out.println("after metadata " + finMetaData);

        System.out.println("before vendor0" + brVendor0);
        ByteRecord finVendor0 = getSanitizedByteRecord(brVendor0);
        System.out.println("after vendor0 " + finVendor0);


        System.out.println("before vendor1" + brVendor1);
        ByteRecord finVendor1 = getSanitizedByteRecord(brVendor1);
        System.out.println("after vendor1 " + finVendor1);


        ByteRecord finHwMsgId = getSanitizedByteRecord(brHwMsgId);
        List<ByteRecord> dr = getSanitizedDataRecord(data);
        MessageRecord fin = new MessageRecord();

        fin.setMetaData(finMetaData);
        fin.setVendorId(finVendor0, 0);
        fin.setVendorId(finVendor1, 1);
        fin.setHwAndMsgId(finHwMsgId);
        fin.setDataBytes(dr);
        finalRecords[msgId] = fin;
    }

    private List<ByteRecord> getSanitizedDataRecord(List<List<ByteRecord>> data) {
        List<ByteRecord> ret = new ArrayList<ByteRecord>();
        int dataLen = data.get(0).size();
        for (int j = 0; j < dataLen ; j++) {
            List<ByteRecord> lbr = new ArrayList<ByteRecord>();
            for (int i = 0; i < data.size(); i++) {
                lbr.add(data.get(i).get(j));
            }
            ByteRecord f = getSanitizedByteRecord(lbr);
            ret.add(f);
            lbr.clear();
        }
        return ret;
    }

    public ByteRecord getSanitizedByteRecord(List<ByteRecord> lst){
        ByteRecord ret = new ByteRecord(8);
        int []data = new int[]{0,0,0,0,0,0,0,0};
        int listsize = lst.size();
        for (int i = 0; i < lst.size(); i++) {
            for (int j = 0; j < data.length; j++) {
                data[j] = data[j] + ( lst.get(i).get(j) ? 1:0);
            }
        }
        System.out.println("Array counted " + Arrays.toString(data) + " " + listsize);

        for (int j = 0; j < data.length; j++) {
            if(data[j] >=  listsize/2)
                ret.set(j);
        }
        return ret;
    }

    /**
     * This goes through all the data records and if it finds any it will
     * @return
     */
    public int[] getMessageIds() {
        processRecords();
        ArrayList<Integer> list = new ArrayList<Integer>();
        System.out.println(" len " + finalRecords.length);
        for (int i = 0 ; i< finalRecords.length; i++) {
            MessageRecord rec = finalRecords[i];
            if(rec != null) {
                list.add(i);
            }
        }

        int[] idList = new int[list.size()];
        int index = 0;
        for (int b : list) {
            idList[index++] = b;
        }
        return idList;
    }

    public MessageRecord getSanitizedRecord(int id){
        processRecords();
        return finalRecords[id];
    }

    public void reset(){
        records = new ArrayList<List<MessageRecord>>(8);
        finalRecords = new MessageRecord[8];
        toBeProcessed = new LinkedList<MessageRecord>();
        for (int i = 0; i < 8; i++) {
            List<MessageRecord> record = new ArrayList<MessageRecord>();
            records.add( record);
        }
    }
}
