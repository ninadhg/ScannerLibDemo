package com.woluxi.scannerlib.data;


import java.util.LinkedList;

/**
 * Created by ninadhg on 11/2/17.
 */

abstract public class LimitedQueueAverage extends LinkedList {
    public abstract double getFPS();
    public abstract double getAvg();
    public abstract void add(double o);
}
