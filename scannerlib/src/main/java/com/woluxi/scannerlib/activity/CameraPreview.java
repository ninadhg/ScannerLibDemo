package com.woluxi.scannerlib.activity;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.woluxi.scannerlib.data.LimitedQueueAverage;
import com.woluxi.scannerlib.data.LimitedQueueAverageWithFPS;
import com.woluxi.scannerlib.data.LimitedQueueAverageWithoutFPS;
import com.woluxi.scannerlib.data.MessageStateMachineController;

import java.io.IOException;
import java.util.List;

/**
 * Created by ninadhg on 2/14/17.
 */

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback{
    public Camera getmCamera() {
        return mCamera;
    }

    Camera mCamera;
    SurfaceHolder mSurfHolder;
    private static final String TAG = "CameraPreview";
    WindowManager mWindowManager;
    LimitedQueueAverage ledStatus ;


    public double getFps() {
        return ledStatus.getFPS();
    }
    public CameraPreview(Context context,boolean enableFPS) {
        super(context);
        init(context);
        if(enableFPS)
            ledStatus = new LimitedQueueAverageWithFPS(80,10.0);
    }

    public CameraPreview(Context context){
        super(context);
        init(context);
    }
    private void init(Context context) {
        ledStatus = new LimitedQueueAverageWithoutFPS(80,10.0);
        mWindowManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mSurfHolder = getHolder();
        mSurfHolder.addCallback(this);
        mSurfHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }
    private Camera.Size m_previewSize = null;
    private byte[][] m_previewBuffers = null;
    private byte[] m_lastPreviewBuffer = null;
    private static final int BUFFER_POOL_SIZE = 2;

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mCamera = Camera.open();
        Camera.Parameters parameters = mCamera.getParameters();
        //parameters.setPreviewSize(width, height);

        // Check what resolutions are supported by your camera
        Camera.Size mSize = getSupportedSize(parameters);
        parameters.setPictureSize(mSize.width, mSize.height);
        parameters.setPreviewSize(mSize.width, mSize.height);

        mCamera.stopPreview();
        mCamera.setParameters(parameters);
        Log.i(TAG, "Chosen resolution at start: "+mSize.width+" "+mSize.height);

        setCameraDisplayOrientation();
        try
        {
            // If did not set the SurfaceHolder, the preview area will be black.
            mCamera.setPreviewDisplay(holder);
            mCamera.setPreviewCallbackWithBuffer(null);

            mCamera.setPreviewCallback(this);
            final Camera.Parameters params = mCamera.getParameters();
            m_previewSize = params.getPreviewSize();
            double bytesPerPixel = ImageFormat.getBitsPerPixel(params.getPreviewFormat()) / 8.0;
            int bufferSizeNeeded = (int) Math.ceil(bytesPerPixel * m_previewSize.width * m_previewSize.height);

            // We could keep the same buffers when they are already bigger than the required size
            // but the Android doc says the size must match, so in doubt just replace them.
            if (m_previewBuffers == null || m_previewBuffers[0].length != bufferSizeNeeded)
                m_previewBuffers = new byte[BUFFER_POOL_SIZE][bufferSizeNeeded];

            // Add callback and queue all buffers
            mCamera.setPreviewCallbackWithBuffer(this);
            for (byte[] buffer : m_previewBuffers)
                mCamera.addCallbackBuffer(buffer);
        }
        catch (IOException e)
        {
            mCamera.release();
            mCamera = null;
        }
    }


    private void setCameraDisplayOrientation(){
        int rotation = mWindowManager.getDefaultDisplay().getRotation();

        Camera.CameraInfo info = new Camera.CameraInfo();
        // camera id is ...
        Camera.getCameraInfo(0, info);
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        mCamera.setDisplayOrientation(result);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Camera.Parameters parameters;
        Log.d(TAG, "surfaceChanged " + format + " width " + width + " height " + height);
        parameters = mCamera.getParameters();

// Check what resolutions are supported by your camera
        Camera.Size mSize = getSupportedSize(parameters);
        parameters.setPictureSize(mSize.width, mSize.height);
        parameters.setPreviewSize(mSize.width, mSize.height);

        Log.i(TAG, "Chosen resolution at surfaceChanged : "+mSize.width+" "+mSize.height);

        //mCamera.stopPreview();
        mCamera.setParameters(parameters);
        mCamera.startPreview();
    }

    private Camera.Size  getSupportedSize(Camera.Parameters parameters){
        List<Camera.Size> sizes = parameters.getSupportedPictureSizes();

// Iterate through all available resolutions and choose one.
// The chosen resolution will be stored in mSize.
        Camera.Size mSize = parameters.getPictureSize();
        for (Camera.Size size : sizes) {
            Log.i(TAG, "Available resolution: "+size.width+" "+size.height);
            if(size.height < 800  && size.width < 800){
                mSize = size;
                break;
            }
        }

        return mSize;
    }
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.setPreviewCallback(null);
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    int counter = 0;
    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (m_lastPreviewBuffer != null)
            camera.addCallbackBuffer(m_lastPreviewBuffer);
        m_lastPreviewBuffer = data;

        Camera.Parameters parameters = camera.getParameters();
        //todo : convert this to a threadpool instead of creating a new thread all the time.
        new StateMachineThread(data, parameters, ledStatus, counter++).start();
    }
}

class StateMachineThread extends Thread {
    private static final String TAG = "StateMachineThread";

    private byte[] data;
    Camera.Parameters parameters;
    LimitedQueueAverage ledStatus;
    PixelFormat pf;
    public StateMachineThread(byte[] data, Camera.Parameters parameters, LimitedQueueAverage ledStatus, int counter) {
        this.data = data;
        this.parameters = parameters;
        this.ledStatus = ledStatus;
        pf = new PixelFormat(); // create a PixelFormat object
        PixelFormat.getPixelFormatInfo(parameters.getPreviewFormat(), pf); // get through the previewFormat-int the PixelFormat
    }

    @Override
    public void run() {
        int previewSizeWidth = parameters.getPreviewSize().width;
        int previewSizeHeight = parameters.getPreviewSize().height;

        int bpp = pf.bytesPerPixel; // save the BytesPerPixel for this Pixelformat
        int bpl = bpp*previewSizeWidth;

        int xStart = previewSizeWidth/2 - 20;
        int xEnd = previewSizeWidth/2 + 20;

        int yStart = previewSizeHeight/2 - 20;
        int yEnd = previewSizeHeight/2 + 20;

        double total = 0;
        for (int column =  xStart; column <xEnd; column = column + 1) {
            for(int row = yStart; row<yEnd; row++) {
                int intensity = data[ (row * bpl) + (column * bpp)]&0xff;
                total = total  + intensity;
            }
        }
        double m = (total/1600.0);
        //Log.d(TAG, " intensity " + m + " avg " + ledStatus.getAvg());
        //ledStatus.add(m, System.currentTimeMillis());
        ledStatus.add(m);
        MessageStateMachineController.INSTANCE.feedMessageStateMachine((m > ( 1.05 * ledStatus.getAvg())));
    }
}