/*
 * Copyright (C) Woluxi, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ninad Ghodke <ninad@woluxi.com>, 2017.
 */

package com.woluxi.scannerlibdemo;

import android.net.wifi.ScanResult;

import java.util.List;

/**
 * Created by ninadhg on 5/4/17.
 * This is the subscribe part of the publish subscribe pattern.
 */

public interface WifiAccumulatorInterface {
    void addSSIDList(List<ScanResult> aplst, List<ScanResult> availRouterlst);
}
