/*
 * Copyright (C) Woluxi, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ninad Ghodke <ninad@woluxi.com>, 2017.
 */

package com.woluxi.scannerlibdemo;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class WifiConnect {
    private static final String TAG = "WifiConnect";

    private static final String PSK = "PSK";
    private static final String WEP = "WEP";
    private static final String OPEN = "Open";

    private static final int REQUEST_ENABLE_WIFI = 10;

    private final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture taskHandler;

    private ScanReceiver scanReceiver;
    private ConnectionReceiver connectionReceiver;
    private volatile String ssidSearchTerm;
    private int timeout;
    Context context;
    List<WifiAccumulatorInterface> crd;

    ArrayList <ScanResult> ssidAPList = new ArrayList<>() ;
    ArrayList <ScanResult> ssidNearbyNetworkList = new ArrayList<>() ;


    WifiConnect(int timeout, Context context){
        this.timeout = timeout;
        this.context = context;
    }

    public void addSSIDListListener(WifiAccumulatorInterface s){
        crd.add(s);
    }

    protected void collectSSIDs(String ssid) {
        this.crd = new ArrayList<>();
        this.ssidSearchTerm = ssid;
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled()) {
            scanAndCollectWifiNetworks();
        } else {
            showWifiDisabledDialog();
        }
    }


    private void showWifiDisabledDialog() {
        new AlertDialog.Builder(context)
                .setCancelable(false)
                .setMessage(context.getString(R.string.wifi_disabled))
                .show();
    }

    /**
     * Get the security type of the wireless network
     *
     * @param scanResult the wifi scan result
     * @return one of WEP, PSK of OPEN
     */
    private String getScanResultSecurity(ScanResult scanResult) {
        final String cap = scanResult.capabilities;
        final String[] securityModes = {WEP, PSK};
        for (int i = securityModes.length - 1; i >= 0; i--) {
            if (cap.contains(securityModes[i])) {
                return securityModes[i];
            }
        }
        return OPEN;
    }

    // User has returned from settings screen. Check if wifi is enabled
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_WIFI && resultCode == 0) {
            WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (wifi.isWifiEnabled() || wifi.getWifiState() == WifiManager.WIFI_STATE_ENABLING) {
                scanAndCollectWifiNetworks();
            }
        }
    }

    /**
     * Start to connect to a specific wifi network
     */
    private void scanAndCollectWifiNetworks() {
        final WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifi.getConnectionInfo();
        Log.d(TAG," scanAndCollectWifiNetworks " +wifiInfo.getSSID()  + " " + ssidSearchTerm);
        if (ConfigureRemoteDevice.INSTANCE.isConnectedToNetwork() && wifiInfo.getSSID().replace("\"", "").equals(ssidSearchTerm)) {
            return;
        }
        taskHandler = worker.schedule(new TimeoutTask(), timeout, TimeUnit.SECONDS);
        scanReceiver = new ScanReceiver(this);
        context.registerReceiver(scanReceiver
                , new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifi.startScan();
    }



    public boolean addSSIDIfNotPresent(ArrayList <ScanResult> ssidList , ScanResult ssid){
        for (ScanResult s: ssidList) {
            if(s.equals(ssid))
                return false;
        }
        ssidList.add(ssid);
        return true;
    }

    public void finishAddingSSID () {
        for( WifiAccumulatorInterface c : crd) {
            //Log.d(TAG, "iterating through the receivers" + c );
            c.addSSIDList(ssidAPList, ssidNearbyNetworkList);
        }
    }

    /**
     * Broadcast receiver for connection related events
     */
    public class ConnectionReceiver extends BroadcastReceiver {
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        @Override
        public void onReceive(Context context, Intent intent) {
            WifiInfo wifiInfo = wifi.getConnectionInfo();
            //Log.d(TAG, "onReceive ConnectionReceiver " + wifiInfo);
            if (ConfigureRemoteDevice.INSTANCE.isConnectedToNetwork()) {
                //Log.d(TAG,"Got connected to " + wifiInfo.getSSID() + " in the connection receiver");
                if (wifiInfo.getSSID().replace("\"", "").contains(ssidSearchTerm)) {
                    context.unregisterReceiver(this);
                    if (taskHandler != null) {
                        taskHandler.cancel(true);
                    }
                } else {
                    //Log.d(TAG,"does not contain the term");
                }
            } else {
                //Log.d(TAG,"not connected");
            }
        }
    }

    /**
     * Broadcast receiver for wifi scanning related events
     */
    public class ScanReceiver extends BroadcastReceiver {
        WifiConnect wf;
        public ScanReceiver(WifiConnect wf){
            this.wf = wf;
        }
        @Override
        public void onReceive(Context context, Intent intent) {
            WifiManager wifi = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
            List<ScanResult> scanResultList = wifi.getScanResults();
            //Log.d(TAG, "input results " + scanResultList);
            for (ScanResult scanResult : scanResultList) {
                if (scanResult.SSID.contains(ssidSearchTerm)) {
                    wf.addSSIDIfNotPresent(ssidAPList, scanResult);
                } else {
                    wf.addSSIDIfNotPresent(ssidNearbyNetworkList, scanResult);

                }
            }
            wf.finishAddingSSID();
            context.unregisterReceiver(this);
            if (taskHandler != null) {
                taskHandler.cancel(true);
            }
        }
    }

    public void connectToNetwork(ScanResult scanResult, String pass, String ssid){
        WifiManager wifi = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        String security = getScanResultSecurity(scanResult);
        this.ssidSearchTerm = ssid;

        // configure based on security
        final WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + scanResult.SSID + "\"";
        switch (security) {
            case WEP:
                conf.wepKeys[0] = "\"" + pass + "\"";
                conf.wepTxKeyIndex = 0;
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                break;
            case PSK:
                conf.preSharedKey = "\"" + pass + "\"";
                break;
            case OPEN:
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                break;
        }
        try {context.unregisterReceiver(connectionReceiver);} catch (Exception e) {} // do nothing
        //Log.d(TAG,"wifi configuration is " + conf + " ssidSearchTerm is " + ssid);
        connectionReceiver = new ConnectionReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        intentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        context.registerReceiver(connectionReceiver, intentFilter);
        int netId = wifi.addNetwork(conf);
        Log.d(TAG, "configuration netID is " + netId);
        wifi.disconnect();
        wifi.enableNetwork(netId, true);
        wifi.reconnect();
    }

    /**
     * Timeout task. Called when timeout is reached
     */
    private class TimeoutTask implements Runnable {
        @Override
        public void run() {
            WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifi.getConnectionInfo();
            Log.d(TAG,"inside timeout task " + ConfigureRemoteDevice.INSTANCE.isConnectedToNetwork()
                    + " " + wifiInfo.getSSID());
            if (ConfigureRemoteDevice.INSTANCE.isConnectedToNetwork() && wifiInfo.getSSID().replace("\"", "").equals(ssidSearchTerm)) {
                try {
                    context.unregisterReceiver(connectionReceiver);
                } catch (Exception ex) {
                    // ignore if receiver already unregistered
                }
            } else {
                try {
                    context.unregisterReceiver(connectionReceiver);
                } catch (Exception ex) {
                    // ignore if receiver already unregistered
                }
            }
        }
    }
}
