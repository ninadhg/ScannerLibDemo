/*
 * Copyright (C) Woluxi, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ninad Ghodke <ninad@woluxi.com>, 2017.
 */

package com.woluxi.scannerlibdemo;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.woluxi.scannerlib.activity.CameraPreview;
import com.woluxi.scannerlib.data.Message.MessageRecord;
import com.woluxi.scannerlib.data.MessageStateMachineCallback;
import com.woluxi.scannerlib.data.MessageStateMachineController;

public class Step2CollectCredentialsCameraActivity extends Activity implements MessageStateMachineCallback {

    private CameraPreview camPreview;
    private FrameLayout mainLayout;
    ProgressBar progressBar;
    TextView lbl;
    TextView fpsValueText;
    TextView fpsLabel;

    private static final String TAG = "Cameractivity";

    private int initialNetworkConfigId;
    private Camera camera;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,"creating camera activity");
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        initialNetworkConfigId = intent.getIntExtra("initialNetworkConfigId", 0);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera_wifi);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

        SurfaceView camView = new SurfaceView(this);
        SurfaceHolder camHolder = camView.getHolder();
        camPreview = new CameraPreview(getApplicationContext(), true);

        camHolder.addCallback(camPreview);
        camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);;

        progressBar.setProgress(0);
        progressBar.setMax(100);
        lbl = (TextView) findViewById(R.id.textView);
        lbl.setText("");

        fpsValueText = (TextView)findViewById(R.id.fpsQualityText);
        fpsValueText.setText("");
        fpsLabel = (TextView)findViewById(R.id.fpsSmallLabel);

        MessageStateMachineController.INSTANCE.registerCallback(this);

        mainLayout = (FrameLayout) findViewById(R.id.activity_camera);
        mainLayout.addView(camView);
    }

    private android.hardware.Camera getCamera() {
        if (camera == null) {
            camera = camPreview.getmCamera();
        }
        return camera;
    }

    public void printFPS(){
        /*

        int fps = (int)camPreview.getFps();
        fpsValueText.setText(String.valueOf(fps));
        if(fps> 20) {
            fpsValueText.setTextColor(Color.GREEN);
            //fpsLabel.setTextColor(Color.GREEN);
        } else if(fps>15){
            fpsValueText.setTextColor(Color.YELLOW);
            //fpsLabel.setTextColor(Color.YELLOW);
        } else {
            fpsValueText.setTextColor(Color.RED);
            //fpsLabel.setTextColor(Color.RED);
        }
*/
    }

    @Override
    public void initComplete() {
        Log.d(TAG, "back to initConfigData");
        progressBar.setProgress(0);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printFPS();
                lbl.setText( "Trying to acquire LED");
            }
        });
    }

    @Override
    public void preambleReadComplete() {
        progressBar.setProgress(20);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printFPS();
                lbl.setText("Starting data acquisition");
            }
        });
    }

    @Override
    public void metaDataReadComplete() {
        progressBar.setProgress(40);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printFPS();
                lbl.setText("MetaData read");
            }
        });
    }

    @Override
    public void vendorReadComplete() {
        progressBar.setProgress(0);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printFPS();
                lbl.setText("Trying to acquire LED");
            }
        });
        // Should not reach this since this is in custom mode.
    }

    @Override
    public void hwMsgIdReadComplete() {
        progressBar.setProgress(0);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printFPS();
                lbl.setText("Trying to acquire LED");
            }
        });
        // Should not reach this since this is in custom mode.
    }

    @Override
    public void dataFirstByteRead() {
        progressBar.setProgress(60);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printFPS();
                lbl.setText( "Starting reading keys");
            }
        });
    }

    @Override
    public void dataReadComplete(MessageRecord br) {
        final Context context = this;
        progressBar.setProgress(100);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                lbl.setText("Finished reading keys");
            }
        });
        final Intent intent = new Intent(getApplicationContext(), Step3GetConfigStatusActivity.class);
        final byte[] data = br.getDataAsBytes();
        byte metadata =  br.getMetaData().getByte();
        ConfigureRemoteDevice cfd = ConfigureRemoteDevice.INSTANCE;
        cfd.initDeviceCredentials(data, metadata);
        cfd.setReconfigureMode(false);
        cfd.completeProvisioning();
        intent.putExtra("initialNetworkConfigId", initialNetworkConfigId);
        startActivity(intent);
        Log.d(TAG, "finished reading the message " + br);
    }
}
