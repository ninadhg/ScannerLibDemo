/*
 *
 *  Copyright (C) Woluxi, Inc - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Ninad Ghodke <ninad@woluxi.com>, 2017.
 *
 */

package com.woluxi.scannerlibdemo;

/**
 * Created by ninadhg on 4/17/17.
 */

public enum ConfigureState {
    CONFIGURATION_STARTED_STATE,
    NO_NETWORKS_FOUND_STATE,
    COULD_NOT_CONNECT_TO_AP_NETWORK_STATE,
    RECONFIGURATION_FAILED,
    CONFIGURATION_FAILED_STATE,
    CONFIGURATION_SUCCESSFUL_STATE,
    CONFIGURATION_IP_ADDRESS_FOUND_STATE
}
