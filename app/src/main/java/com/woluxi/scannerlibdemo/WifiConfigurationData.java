/*
 * Copyright (C) Woluxi, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ninad Ghodke <ninad@woluxi.com>, 2017.
 */

package com.woluxi.scannerlibdemo;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.util.Log;

import java.util.List;

/**
 * Created by ninadhg on 5/4/17.
 */

public enum WifiConfigurationData  implements WifiAccumulatorInterface{
    INSTANCE;

    List<ScanResult> ssidAPList;
    private static final String TAG = "WifiConfigurationData";

    List <ScanResult> nearbyNetworkSSIDList;
    boolean SSIDScanned;
    boolean initialized = false;

    void init(Context context) {
        Log.d(TAG, "initializing the ssid list");
        synchronized (this) {
            if(initialized)
                return;
            //collect all the wifi networks starting with Woluxi_
            WifiConnect wf = new WifiConnect( 10, context);
            wf.collectSSIDs("Woluxi_");
            wf.addSSIDListListener(this);
        }
    }

    /**
     * This function stores a list of all the wifi access points found, and is also
     * a signal that the scan has completed.
     * @param aplst
     * @param availRouterlst
     */
    public void addSSIDList(List<ScanResult> aplst, List<ScanResult> availRouterlst) {
        this.ssidAPList = aplst;
        Log.d(TAG, "Length of availRouterlst array list " + availRouterlst.size());
        this.nearbyNetworkSSIDList = availRouterlst;
        SSIDScanned = true;
    }

    public List<ScanResult> getAPSSIDList() {
        return ssidAPList;
    }
    public List<ScanResult> getNearbyNetworkSSIDList() {
        return nearbyNetworkSSIDList;
    }
}
