/*
 *
 *  Copyright (C) Woluxi, Inc - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Ninad Ghodke <ninad@woluxi.com>, 2017.
 *
 */

package com.woluxi.scannerlibdemo;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.AutoCompleteTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Step1CollectConfigActivity extends AppCompatActivity{
    private static final String TAG = "Step1";
    public static final String PREFS_NAME = "PrefsFile";

    private AutoCompleteTextView homeSSID;
    private TextView homeSSIDPassword;
    private Button submitButton;
    private CheckBox cb;
    private CheckBox sshcb;
    private CheckBox piPasswordcb;
    private TextView piPassword;

    private WifiConfigurationData wc;
    int initialNetworkConfigId = -1;

    public final static int REQUEST_CODE_WF_COMPLETION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_configuration);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForPermission();
            }
        }
        startAct();
    }

    private boolean checkIfPreviouslyConfigured(String currentSSID, String savedSSID) {
        Log.d(TAG, "checkIfPreviouslyConfigured " + savedSSID);
        if( (savedSSID == null) || (currentSSID == null) )
            return false;
        if(currentSSID.contains(savedSSID)){
            Log.d(TAG,"checkIfPreviouslyConfigured true" );
            return true;
        }
        return false;
    }

    private String [] createNearbySSIDStringArray(List<ScanResult>inp){
        ArrayList<String> retarray = new ArrayList<>();
        for(int i = 0 ; i< inp.size(); i++){
            if( inp.get(i).SSID.length() > 0 )
                retarray.add(new String(inp.get(i).SSID));
        }
        String ret[] = new String[retarray.size()];
        Log.d(TAG, Arrays.toString(retarray.toArray()));
        return  retarray.toArray(ret);
    }


    void populateOnScreenSSID( List<ScanResult> availRouterlst) {
        //XXX: if the user is fast enough we might not have a router list.
        if ((availRouterlst != null) && availRouterlst.size() > 0 ) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                    createNearbySSIDStringArray(availRouterlst));
            homeSSID.setThreshold(1);
            homeSSID.showDropDown();
            homeSSID.setAdapter(adapter);

            homeSSID.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                        long id) {
                    populateOnScreenPassword();
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            });
        }
        homeSSID.setTextColor(Color.BLACK);
    }

    void populateOnScreenPassword(){
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String ssidPassword = settings.getString(homeSSID.getText() + "_pass", "");
        if(!ssidPassword.isEmpty()) {
            CheckBox scb = (CheckBox)findViewById(R.id.home_wifi_save_password_checkbox);
            scb.setChecked(true);
            homeSSIDPassword.setText(ssidPassword);
        }
    }

    void startAct() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if(!wifiManager.getConnectionInfo().getSSID().contains("Woluxi_")) {
            initialNetworkConfigId = wifiManager.getConnectionInfo().getNetworkId();
            Log.d(TAG, "initial network config ID " + initialNetworkConfigId);
        }

        ConfigureRemoteDevice cfd = ConfigureRemoteDevice.INSTANCE;
        cfd.initContext(this);
        cfd.collectSSIDListAndConnectToAP();
        wc = WifiConfigurationData.INSTANCE;
        homeSSID = (AutoCompleteTextView) findViewById(R.id.homeSSID);
        homeSSID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"on click listener for ssid");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        populateOnScreenSSID(wc.getNearbyNetworkSSIDList());
                    }
                });
            }
        });
        //todo: Set a finish editing callback which will populate the password immediately.
        homeSSID.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    populateOnScreenPassword();
                }
            }
        });
        homeSSIDPassword = (TextView) findViewById(R.id.homeSSIDPassword);

        sshcb = (CheckBox)findViewById(R.id.enable_ssh_checkbox);
        piPasswordcb = (CheckBox)findViewById(R.id.enable_pi_password_checkbox);
        piPassword = (TextView)findViewById(R.id.piPassword);

        cb = (CheckBox)findViewById(R.id.home_wifi_show_password_checkbox);
        cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cb.isChecked()){
                    homeSSIDPassword.setTransformationMethod(null);
                }else{
                    homeSSIDPassword.setTransformationMethod(new PasswordTransformationMethod());
                }
            }
        });

        submitButton = (Button) findViewById(R.id.to_camera_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!ConfigureRemoteDevice.INSTANCE.isAdHocNetworkConnected()) {
                    AlertDialog alertDialog = new AlertDialog.Builder(Step1CollectConfigActivity.this).create();
                    alertDialog.setTitle("Join Woluxi_* WiFi Network");
                    alertDialog.setMessage(getString(R.string.ad_hoc_wifi_not_connected));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS),REQUEST_CODE_WF_COMPLETION);
                                    //todo rescan the ssidlist again since the Woluxi_ network was not seen.
                                }
                            });
                    alertDialog.show();
                    return;
                }
                int ssidpasswordlen = homeSSIDPassword.getText().length();
                if((ssidpasswordlen > 0) &(ssidpasswordlen < 8)){
                    Log.d(TAG,"incorrect password");
                    AlertDialog alertDialog = new AlertDialog.Builder(Step1CollectConfigActivity.this).create();
                    alertDialog.setTitle("SSID password x");
                    alertDialog.setMessage(getString(R.string.password_length_short));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    return;
                }
                startConfigurationActivity();

            }
        });
        String hsid = ConfigureRemoteDevice.INSTANCE.getCurrentSsid(this);
        if((hsid!= null) && !hsid.contains("Woluxi_")) {
            homeSSID.setText(hsid.substring(1, hsid.length() - 1));
            populateOnScreenPassword();
        }
    }

    private void startConfigurationActivity(){
        String homeWifiSSID = homeSSID.getText().toString();
        String homeWifiPassword = homeSSIDPassword.getText().toString();
        CheckBox scb = (CheckBox)findViewById(R.id.home_wifi_save_password_checkbox);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        if (scb.isChecked()) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(homeWifiSSID+"_pass", homeSSIDPassword.getText().toString());
            editor.commit();
        }
        ConfigureRemoteDevice cfd = ConfigureRemoteDevice.INSTANCE;
        cfd.initConfigData(homeWifiSSID, homeWifiPassword,
                sshcb.isChecked(), piPasswordcb.isChecked(), piPassword.getText().toString());

        // check if the ssid is present in saved configuration.
        String currentSSID = ConfigureRemoteDevice.INSTANCE.getCurrentSsid(this);
        Log.d(TAG, "config entry savedssid " + settings.getString("savedAPSSID", null) + " current " + currentSSID);

        if (checkIfPreviouslyConfigured(currentSSID, settings.getString("savedAPSSID", null))) {
            cfd.setReconfigureMode(true);
            final Intent intent = new Intent(getApplicationContext(), Step3GetConfigStatusActivity.class);
            intent.putExtra("initialNetworkConfigId", initialNetworkConfigId);
            cfd.completeProvisioning();
            startActivity(intent);
            return;
        } else {
            Intent intent = new Intent(getApplicationContext(), Step2CollectCredentialsCameraActivity.class);
            intent.putExtra("initialNetworkConfigId", initialNetworkConfigId);
            Log.d(TAG, "pre config camera activity ssid " + homeSSID.getText() + " passwd " + homeSSIDPassword.getText());
            startActivity(intent);
        }
    }
    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;

    }

    private void requestForPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startAct();
                } else {
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case REQUEST_CODE_WF_COMPLETION:
                if (ConfigureRemoteDevice.INSTANCE.isAdHocNetworkConnected()){
                    WifiConfigurationData.INSTANCE.init(getApplicationContext());
                    startConfigurationActivity();
                }else {
                    AlertDialog alertDialog = new AlertDialog.Builder(Step1CollectConfigActivity.this).create();
                    alertDialog.setTitle("Not connected to Woluxi_ Network");
                    alertDialog.setMessage("To proceed in the configuration process connection to WiFi network starting with Woluxi_ is required");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                break;
            default:
                break;
        }
    }
}
