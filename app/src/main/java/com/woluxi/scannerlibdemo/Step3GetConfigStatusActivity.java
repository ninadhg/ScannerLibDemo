/*
 * Copyright (C) Woluxi, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ninad Ghodke <ninad@woluxi.com>, 2017.
 */

package com.woluxi.scannerlibdemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Step3GetConfigStatusActivity extends AppCompatActivity  {
    private static final String TAG = "GetStatus";
    private static final String STATUS_TEXT_VIEW_KEY = "status_text_view";
    private static final String IPADDRESS_TEXT_VIEW_KEY = "ipaddress_text_view";
    private static final String PROGRESSBAR_VIEW_KEY = "progressbar_view";
    private static final String REASON_TEXT_VIEW_KEY = "reason_text_view";


    private Button retryButton;
    private Button quitButton;

    public TextView ipAddressTextView;
    public TextView reasonTextView;
    public TextView statusTextView;
    private ProgressBar progressBar;

    private final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();
    private  int initialNetworkConfigId;

    public Activity getActivity() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialNetworkConfigId = getIntent().getIntExtra("initialNetworkConfigId", -1);
        setContentView(R.layout.activity_configuration_status);

        retryButton = (Button) findViewById(R.id.retry_button);
        quitButton = (Button)findViewById(R.id.quit_button);
        progressBar = (ProgressBar)findViewById(R.id.status_progress_bar);
        ipAddressTextView = (TextView)findViewById(R.id.ip_address_textview);
        reasonTextView = (TextView)findViewById(R.id.reason_text_view);
        statusTextView = (TextView)findViewById(R.id.config_status_textview);

        retryButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ConfigureRemoteDevice.INSTANCE.isAdHocNetworkConnected()) {
                    Intent intent = new Intent(getApplicationContext(), Step2CollectCredentialsCameraActivity.class);
                    intent.putExtra("initialNetworkConfigId", initialNetworkConfigId);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), Step1CollectConfigActivity.class);
                    startActivity(intent);
                }
            }
        });
        quitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //// TODO: 5/30/17 delete all the receivers (Scan and connection) 
                finishAffinity();
            }
        });
    }

    public void reconnectToOriginalNetwork(int originalNetId ){
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiManager.disconnect();
        if(originalNetId == -1) {
            wifiManager.reconnect();
            return;
        }
        Log.d(TAG, "Connecting back to original network " + originalNetId);
        wifiManager.enableNetwork(originalNetId, false);
        wifiManager.reconnect();
    }

    @Override
    public void onResume() {
        super.onResume();
        // This registers mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mMessageReceiver,
                        new IntentFilter("woluxiWifiProvisioningStatus"));
    }

    // Handling the received Intents for the "my-integer" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast toast;
            int duration = Toast.LENGTH_SHORT;

            // Extract data included in the Intent
            ConfigureState status = (ConfigureState)intent.getSerializableExtra("ConfigureState");
            switch (status ) {
                case NO_NETWORKS_FOUND_STATE:
                    statusTextView.setText("No Ad-Hoc networks starting with Woluxi_ were found");
                    statusTextView.setTextColor(Color.RED);
                    new AlertDialog.Builder(Step3GetConfigStatusActivity.this)
                            .setCancelable(true)
                            .setMessage(String.format(context.getString(R.string.wifi_not_found), "Woluxi_*"))
                            .show();
                    progressBar.setVisibility(View.INVISIBLE);
                    reconnectToOriginalNetwork(initialNetworkConfigId);
                    ipAddressTextView.setText("");
                    break;
                case COULD_NOT_CONNECT_TO_AP_NETWORK_STATE:
                    statusTextView.setText("Could not connect to Ad-Hoc network starting with Woluxi_. Please try again after manually connecting to the SSID starting with Woluxi_");
                    statusTextView.setTextColor(Color.RED);
                    ipAddressTextView.setText("");

                    break;
                case CONFIGURATION_STARTED_STATE:
                    statusTextView.setText("Configuration Started");
                    break;
                case RECONFIGURATION_FAILED:
                    ConfigureRemoteDevice.INSTANCE.setReconfigureMode(false);
                    Intent configIntent = new Intent(getApplicationContext(), Step2CollectCredentialsCameraActivity.class);
                    configIntent.putExtra("initialNetworkConfigId", initialNetworkConfigId);
                    Log.d(TAG, "Starting fresh configuration activity since the reconfig failed");
                    CharSequence text = "Reconfiguration failed!";
                    toast = Toast.makeText(context, text, duration);
                    toast.setGravity(Gravity.TOP| Gravity.CENTER, 0, 10);
                    toast.show();
                    startActivity(configIntent);
                    break;
                case CONFIGURATION_SUCCESSFUL_STATE:
                    ConfigureRemoteDevice.INSTANCE.release();
                    reconnectToOriginalNetwork(initialNetworkConfigId);
                    final String macAddress = intent.getStringExtra("macaddress");
                    statusTextView.setText("Success");
                    statusTextView.setTextColor(Color.BLACK);
                    progressBar.setVisibility(View.GONE);
                    reasonTextView.setText("macaddress : " + macAddress);
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            Log.d(TAG, "Getting configuration status");
                            new IPAddressMulticastReceiver(getActivity(), macAddress).execute();
                        }
                    }, 15000);
                    toast = Toast.makeText(context, "Reconnecting to original WiFi network", duration);
                    toast.setGravity(Gravity.CENTER, 0, 5);
                    toast.show();
                    break;
                case CONFIGURATION_FAILED_STATE:
                    progressBar.setVisibility(View.INVISIBLE);
                    statusTextView.setText("Configuration Failed");
                    statusTextView.setTextColor(Color.RED);
                    ipAddressTextView.setText("");
                    //reconnectToOriginalNetwork(initialNetworkConfigId);
                    progressBar.setVisibility(View.GONE);
                    break;
                case CONFIGURATION_IP_ADDRESS_FOUND_STATE:
                    String msg = intent.getStringExtra("ipaddress");
                    Log.d(TAG," got ip address " + msg);
                    ipAddressTextView.setText(msg);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState "+ ipAddressTextView.getText());
        outState.putString(STATUS_TEXT_VIEW_KEY, statusTextView.getText().toString());
        outState.putString(IPADDRESS_TEXT_VIEW_KEY, ipAddressTextView.getText().toString());
        outState.putString(REASON_TEXT_VIEW_KEY, reasonTextView.getText().toString());

        outState.putBoolean(PROGRESSBAR_VIEW_KEY, progressBar.getVisibility() == View.VISIBLE);
        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG,"restoring instance " + ipAddressTextView.getText());
        statusTextView.setText(savedInstanceState.getString(STATUS_TEXT_VIEW_KEY));

        ipAddressTextView.setText(savedInstanceState.getString(IPADDRESS_TEXT_VIEW_KEY));
        reasonTextView.setText(savedInstanceState.getString(REASON_TEXT_VIEW_KEY));

        progressBar.setVisibility(savedInstanceState.getBoolean(PROGRESSBAR_VIEW_KEY) ?View.VISIBLE:View.GONE);
    }
}
