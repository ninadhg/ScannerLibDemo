/*
 *
 *  Copyright (C) Woluxi, Inc - All Rights Reserved
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *   Written by Ninad Ghodke <ninad@woluxi.com>, 2017.
 *
 */

package com.woluxi.scannerlibdemo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Looper;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static com.woluxi.scannerlibdemo.Step1CollectConfigActivity.PREFS_NAME;

/**
 * Created by ninadhg on 4/13/17.
 */

public enum  ConfigureRemoteDevice{
    INSTANCE;

    private static final String TAG = "CfgRemoteDevice";
    private static final String IPAddress = "192.168.173.1";

    private String homeWifiSSID;
    private String homeWifiPassword;
    private boolean sshcb;
    private boolean piPasswordcb;
    private String piPassword;
    private byte [] data;
    private Context context;
    private String deviceWifiPassword;
    private String devicePublicKey;

    private boolean isReconfigureMode;
    private String fingerprintPublicKey;
    private PublicKey phonePublicKey;
    private PrivateKey phonePrivateKey;
    private String macAddress;
    private ConnectivityManager.NetworkCallback mNetworkCallback;
    /**
     * The initializer also creates the credentials that would be used later.
     *
     * @param homeWifiSSID
     * @param homeWifiPassword
     * @param sshcb
     * @param piPasswordcb
     * @param piPassword
     */
    public void initConfigData(String homeWifiSSID,
                               String homeWifiPassword,
                               boolean sshcb,
                               boolean piPasswordcb,
                               String piPassword) {
        this.homeWifiSSID = homeWifiSSID;
        this.homeWifiPassword = homeWifiPassword;
        this.sshcb = sshcb;
        this.piPasswordcb = piPasswordcb;
        this.piPassword = piPassword;
        this.isReconfigureMode = false;
        intializeKeyStore();
    }
    private void alwaysPreferNetworksWith(@NonNull int[] capabilities, @NonNull int[] transportTypes) {

        NetworkRequest.Builder request = new NetworkRequest.Builder();
        // add capabilities
        for (int cap: capabilities) {
            request.addCapability(cap);
        }

        // add transport types
        for (int trans: transportTypes) {
            request.addTransportType(trans);
        }

        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);

        mNetworkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                try {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        ConnectivityManager.setProcessDefaultNetwork(network);
                    } else {
                        connectivityManager.bindProcessToNetwork(network);
                    }
                } catch (IllegalStateException e) {
                    Log.e(TAG, "ConnectivityManager.NetworkCallback.onAvailable: ", e);
                }
            }
        };
        connectivityManager.registerNetworkCallback(request.build(),mNetworkCallback);
    }

    public void release(){
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            connectivityManager.bindProcessToNetwork(null);
            connectivityManager.unregisterNetworkCallback(mNetworkCallback);
        } else {
            ConnectivityManager.setProcessDefaultNetwork(null);
        }
    }

    void initContext(Context context) {
        int[] capabilities = new int[]{ NetworkCapabilities.NET_CAPABILITY_INTERNET };
        int[] transportTypes = new int[]{ NetworkCapabilities.TRANSPORT_WIFI };

        this.context = context;
        alwaysPreferNetworksWith(capabilities, transportTypes);
    }

    void initDeviceCredentials(byte[] data,
                               byte metadata){
        this.data = data;
        //Log.d(TAG, "Constructor " + metadata + " and " + (metadata & 0x08));
        if((metadata & 0x08) == 0x08) {
            deviceWifiPassword = "Woluxi_" + String.format("%02x",data[0]);
        } else {
            deviceWifiPassword = "";
        }
    }

    void intializeKeyStore(){
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);

            String alias = "ssidprivkey";

            int nBefore = keyStore.size();

            // Create the keys if necessary
            if (!keyStore.containsAlias(alias)) {
                //Log.d(TAG, "generating a new key");
                Calendar notBefore = Calendar.getInstance();
                Calendar notAfter = Calendar.getInstance();
                notAfter.add(Calendar.YEAR, 2);
                KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                        .setAlias(alias)
                        .setKeyType(KeyProperties.KEY_ALGORITHM_RSA)
                        .setSubject(new X500Principal("CN=woluxi"))
                        .setSerialNumber(BigInteger.ONE)
                        .setStartDate(notBefore.getTime())
                        .setEndDate(notAfter.getTime())
                        .build();
                KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
                generator.initialize(spec);
                KeyPair keyPair = generator.generateKeyPair();
            }
            int nAfter = keyStore.size();
            Log.v(TAG, "Before = " + nBefore + " After = " + nAfter);

            // Retrieve the keys
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(alias, null);
            phonePrivateKey = privateKeyEntry.getPrivateKey();
            phonePublicKey =  privateKeyEntry.getCertificate().getPublicKey();

            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] shapub = md.digest(phonePublicKey.getEncoded());

            StringBuilder fingerprintPhonePublicKey = new StringBuilder();
            for (int i=0;i<shapub.length;i++) {
                String appendString = Integer.toHexString(0xFF & shapub[i]);
                if(appendString.length()==1) fingerprintPhonePublicKey.append("0");
                fingerprintPhonePublicKey.append(appendString);
            }
            fingerprintPublicKey = fingerprintPhonePublicKey.toString();
            //Log.d(TAG, "hash pub: "+ fingerprintPhonePublicKey.toString());
        } catch (NoSuchAlgorithmException | CertificateException | NoSuchProviderException | KeyStoreException | InvalidAlgorithmParameterException | IOException | UnsupportedOperationException | UnrecoverableEntryException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    private void collectSSIDs(){
        WifiConfigurationData wc = WifiConfigurationData.INSTANCE;
        wc.init(this.context);
        while(!wc.SSIDScanned) {
            try {
                //Log.d(TAG, "waiting till ssid scanned");
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isConnectedToNetwork() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo!= null && (networkInfo.getType() == ConnectivityManager.TYPE_WIFI)
                    && (networkInfo.getState().equals(NetworkInfo.State.CONNECTED))) {
                    return true;
                }
            }
        }else {
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo != null && (anInfo.getType() == ConnectivityManager.TYPE_WIFI)
                                && (anInfo.getState() == NetworkInfo.State.CONNECTED)) {
                            //Log.d(TAG, "NETWORKNAME: " + anInfo.getTypeName());
                            return true;
                        }
                    }
                }
            }
        }
        //Toast.makeText(mContext,mContext.getString(R.string.please_connect_to_internet), Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean isConnectedToAdHocNetwork(String targetSSID){
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        WifiInfo wi = wifiManager.getConnectionInfo();
        String currentSSID = wi.getSSID();
        currentSSID = currentSSID.replace("\"", "");
        Log.d(TAG, "connected to " + currentSSID + " targetSSID " + targetSSID );
        Log.d(TAG, "determined we are connected to the required network " + currentSSID
                + "  target " + targetSSID + " connection status " + wi);
        return isConnectedToNetwork() && targetSSID.equals(currentSSID);
    }

    private boolean sendProvisioningInfoToWebservice(){
        boolean configured = false;
        try {
            if(isReconfigureMode) {
                if(sendReConfigureInformation()){
                    configured = true;
                } else {
                    deleteAPInfo();
                    transmitStatus(configured?ConfigureState.CONFIGURATION_SUCCESSFUL_STATE:ConfigureState.RECONFIGURATION_FAILED);
                }
            }else {
                if (obtainDevicePublicKey()) {
                    if (sendConfigInformation()) {
                        configured = true;
                    }
                } //else we connected to the wrong network try with new network
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return configured;
    }
    public void completeProvisioning() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean configured = false;
                List<ScanResult> ssidList = WifiConfigurationData.INSTANCE.getAPSSIDList();

                Looper.prepare();
                //XXX: No matching wifi networks were found, this condition cannot occur.
                if(ssidList.size() == 0 ){
                    Log.d(TAG,"No network found");
                    transmitStatus(ConfigureState.NO_NETWORKS_FOUND_STATE);
                    configured = sendProvisioningInfoToWebservice();
                }
                if(ssidList.size() == 1) {
                    configured = sendProvisioningInfoToWebservice();
                } else {
                    int i = 0;
                    //setMobileDataEnabled(context, false);
                    // We will need to check all the ssids with Woluxi_ in them to see which credentials match.
                    while (i < ssidList.size()) {
                        Log.d(TAG, "connecting to network " + ssidList.get(i).SSID);
                        WifiConnect wf = new WifiConnect(10, context);
                        String targetSSID = ssidList.get(i).SSID;
                        // check if we are connected to the wifi network.
                        int try_count = 0;
                        while (true) {
                            Log.d(TAG, "waiting for network " + ssidList.get(i).SSID);
                            if (isConnectedToAdHocNetwork(targetSSID)) {
                                break;
                            }
                            try {
                                Thread.sleep(1000);
                                if (try_count++ > 20) {
                                    transmitStatus(ConfigureState.COULD_NOT_CONNECT_TO_AP_NETWORK_STATE);
                                    return;
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        configured = sendProvisioningInfoToWebservice();
                        if (configured)
                            break;
                        i++;
                    }
                }
                transmitStatus(configured?ConfigureState.CONFIGURATION_SUCCESSFUL_STATE:ConfigureState.CONFIGURATION_FAILED_STATE);
            }
        }).start();
    }


    private void transmitStatus(ConfigureState st){
        Intent intent = new Intent("woluxiWifiProvisioningStatus");
        intent.putExtra("ConfigureState", st);
        if(st == ConfigureState.CONFIGURATION_SUCCESSFUL_STATE)
            intent.putExtra("macaddress", macAddress);
        LocalBroadcastManager.getInstance(this.context).sendBroadcast(intent);
    }

    public HttpURLConnection  createURLConnection() throws IOException {
        HttpURLConnection urlConnection;
        URL url = new URL("http://" + IPAddress + ":9090/");
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setDoOutput(true);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/json");
        urlConnection.setRequestProperty("Accept", "application/json");
        urlConnection.setConnectTimeout(10000);
        urlConnection.connect();
        return urlConnection;
    }

    /**
     * This function will obtain the public key and also verify that the token we obtained is for
     * the current device only.
     * @return
     */
    public boolean obtainDevicePublicKey(){
        BufferedReader reader = null;
        String JsonResponse;
        HttpURLConnection urlConnection = null;
        String uuid = UUID.randomUUID().toString();
        final JSONObject post_dict = new JSONObject();
        //Log.d(TAG, "obtainDevicePublicKey got uuid as " + uuid);

        try {
            post_dict.put("cmd","get_public_key");

            final JSONObject data_dict = new JSONObject();
            data_dict.put("challenge", uuid);
            //Log.d(TAG, "data json" + data_dict);
            String dstring = data_dict.toString();
            post_dict.put("data", getEncodedEncrypted(bufferKey(data, 16), dstring));
            String JsonDATA = post_dict.toString();

            //Log.d(TAG,"data for webserver " + JsonDATA);
            urlConnection = createURLConnection();
            Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
            writer.write(JsonDATA);
            writer.close();
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                Log.d(TAG, "Empty input stream");
                return false;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String inputLine;
            while ((inputLine = reader.readLine()) != null)
                buffer.append(inputLine).append("\n");
            if (buffer.length() == 0) {
                // Stream was empty. No point in parsing.
                Log.d(TAG, "Empty data");
                return false;
            }

            JsonResponse = buffer.toString();
            //Log.i(TAG, "get public key ret " + JsonResponse);
            JSONObject result = new JSONObject(JsonResponse);
            String challengeAns = result.getString("challenge_answer");
            devicePublicKey = result.getString("public_key");
            //Log.d(TAG, "public key array " + devicePublicKey);
            if(challengeAnswered(challengeAns, uuid)){
                //Log.d(TAG, devicePublicKey);
                return true;
            } else
                return false;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(TAG, "Error closing stream", e);
                }
            }
        }
        return false ;
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    // implement such that we check the sha256 hash.
    private boolean challengeAnswered(String challengeAns, String uuid) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(uuid.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();
            //Log.d(TAG , "digest generated  array" + Arrays.toString(md.digest()));

            byte[] ans = hexStringToByteArray(challengeAns);
            //Log.d(TAG , "digest from server array" + Arrays.toString(ans));
            return Arrays.equals(digest, ans);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean checkReturnCreateConfig(InputStream inputStream) throws JSONException {
        BufferedReader reader;

        StringBuffer buffer = new StringBuffer();
        if (inputStream == null) {
            return false;
        }
        reader = new BufferedReader(new InputStreamReader(inputStream));

        String inputLine;
        try {
            while ((inputLine = reader.readLine()) != null)
                buffer.append(inputLine).append("\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(TAG, "Error closing stream", e);
                }
            }
        }
        //Log.d(TAG, "configuration return " + buffer);
        if (buffer.length() == 0) {
            // Stream was empty. No point in parsing.
            return false;
        }
        String JsonResponse;

        JsonResponse = buffer.toString();
        //Log.i(TAG,"json response" + JsonResponse);
        if(JsonResponse == null)
            return false;
        JSONObject jObject;
        jObject = new JSONObject(JsonResponse);
        String status = jObject.getString("status");
        macAddress = jObject.getString("mac_address");
        if(status.equalsIgnoreCase("OK")){
            storeAPInfo(devicePublicKey);
            return true;
        }
        return true;
    }

    private JSONObject createConfigJSON(String pubKey) throws JSONException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException {
        final JSONObject post_dict = new JSONObject();
        JSONObject data_dict;
        data_dict = new JSONObject();

        JSONObject wifi = new JSONObject();
        wifi.put("ssid",homeWifiSSID);
        wifi.put("password", homeWifiPassword);

        data_dict.put("wifi", wifi);
        if(sshcb)
            data_dict.put("ssh_enable", true);
        if(piPasswordcb)
            data_dict.put("pi_password", piPassword);
        if(!isReconfigureMode) {
            post_dict.put("cmd","set_config");
            data_dict.put("led", Base64.encodeToString(data, Base64.DEFAULT));
            try {
                X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(phonePublicKey.getEncoded());
                String pStr = new String(Base64.encode(x509EncodedKeySpec.getEncoded(), Base64.NO_WRAP), "UTF-8");
                //Log.d(TAG, "phonePublicKey " + pStr);
                data_dict.put("public_key", pStr);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            post_dict.put("cmd","set_reconfig");
        }
        //Log.d(TAG, "data json" + data_dict);

        byte[] keyStart = "woluxi start key".getBytes();
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed(keyStart);
        kgen.init(128, sr);
        SecretKey skey = kgen.generateKey();
        byte[] key = skey.getEncoded();

        post_dict.put("secret_key", Base64.encodeToString(
                RSAEncrypt(pubKey, skey.getEncoded()), Base64.DEFAULT));
        String dstring = data_dict.toString();
        //Log.d(TAG, "dstring " + dstring);
        post_dict.put("data", getEncodedEncrypted(key, dstring));
        if(isReconfigureMode) {
            Signature sig = Signature.getInstance("SHA256withRSA");
            sig.initSign(phonePrivateKey);
            try {
                sig.update(dstring.getBytes());
                byte[] signatureBytes = sig.sign();
                //Log.d(TAG, "signature bytes " + Arrays.toString(signatureBytes));

                //Log.d(TAG, "signature:" + Base64.encodeToString(signatureBytes, Base64.DEFAULT));
                post_dict.put("signature", Base64.encodeToString(signatureBytes, Base64.DEFAULT));
                post_dict.put("public_key_id", fingerprintPublicKey);
            } catch (SignatureException e) {
                e.printStackTrace();
            }
        }
        return post_dict;
    }

    private boolean sendConfigInformation() throws java.net.SocketException{
        HttpURLConnection urlConnection = null;
        try {
            final JSONObject post_dict = createConfigJSON(devicePublicKey);
            String JsonDATA = post_dict.toString() ;
            //Log.d(TAG,"data for webserver " + JsonDATA);
            urlConnection = createURLConnection();
            Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
            writer.write(JsonDATA);
            writer.close();
            InputStream inputStream = urlConnection.getInputStream();
            return checkReturnCreateConfig(inputStream);
        } catch (IOException | JSONException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return  false;
    }

    private boolean sendReConfigureInformation() {
        HttpURLConnection urlConnection = null;
        try {
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            String pubkey = settings.getString("savedAPPubKey", "");
            String savedssid = settings.getString("savedAPSSID", "nossid");
            //Log.d(TAG, "ssid " + homeWifiSSID + " passwd " + homeWifiPassword + "saved public key" + pubkey  +  " ssid " + savedssid);
            final JSONObject post_dict = createConfigJSON(pubkey);
            String JsonDATA = post_dict.toString() ;
            //Log.d(TAG,"data for webserver reconfigure " + JsonDATA);
            urlConnection = createURLConnection();
            Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
            writer.write(JsonDATA);
            writer.close();
            InputStream inputStream = urlConnection.getInputStream();
            return checkReturnCreateConfig(inputStream);
        } catch (IOException | JSONException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return  false;
    }

    private void storeAPInfo(String publicKey) {
        String currentSSID = getCurrentSsid(context);
        //Log.d(TAG, "currentSSID " +  currentSSID + " storeAPInfo " + publicKey + " isReconfigmode " + isReconfigureMode);
        if(isReconfigureMode )
            return;
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("savedAPSSID", currentSSID);
        editor.putString("savedAPPubKey", publicKey);
        editor.commit();
    }

    private void deleteAPInfo(){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove("savedAPSSID");
        editor.remove("savedAPPubKey");
        editor.commit();
    }

    /**
     * This function buffers the key to the size that is required by the encryption algorithm
     * This is required because the data we extract out of led is too small.
     * @param data
     * @param size
     * @return
     */
    private byte[] bufferKey(byte []data, int size){
        byte[] out = new byte[size];
        int stuffed = 0;
        int inpSize = data.length;
        while (stuffed < size){
            if((stuffed + inpSize) > size){
                inpSize = size - stuffed;
            }
            System.arraycopy(data, 0, out, stuffed, inpSize);
            stuffed = stuffed + inpSize;
        }
        Log.d(TAG , "Buffered key generated" + Arrays.toString(out));
        return out;
    }

    private byte[] RSAEncrypt(String pubKey, final  byte []plain) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        //Log.d(TAG, "size of public key " + plain.length  + pubKey );
        X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(pubKey, Base64.DEFAULT));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        try {
            PublicKey pkey = kf.generatePublic(spec);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, pkey);
            return cipher.doFinal(plain);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return  null;
    }

    private String getEncodedEncrypted(byte[] data, String data_dict) {
        try {
            SecureRandom random = new SecureRandom();
            byte[] iv = new byte[16];
            random.nextBytes(iv);
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            Cipher c = Cipher.getInstance("AES/CFB/PKCS5Padding");
            Key key = new SecretKeySpec(data, "AES");
            //Log.d(TAG, "key " + Arrays.toString(key.getEncoded())+ " len " + key.getFormat());
            c.init(Cipher.ENCRYPT_MODE, key, ivSpec);
            byte []ret = c.doFinal(data_dict.getBytes());

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
            outputStream.write(iv);
            outputStream.write(ret);

            //Log.d(TAG, "encrypted " + Arrays.toString(outputStream.toByteArray()) + " len " + outputStream.size());
            String base64 = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
            //Log.d(TAG, "base64 encoded " + base64);
            return base64;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void collectSSIDListAndConnectToAP() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //collect ssidlist in a thread
                collectSSIDs();
                //start connecting to AP if only 1 is present.
                if(WifiConfigurationData.INSTANCE.getAPSSIDList().size() == 1) {
                    WifiConnect wf = new WifiConnect(10, context);
                    wf.connectToNetwork(WifiConfigurationData.INSTANCE.getAPSSIDList().get(0), deviceWifiPassword, "Woluxi_");
                }
            }
        }).start();

    }
    public String getCurrentSsid(Context context) {
        String ssid;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo == null) {
            Log.d(TAG,"networkInfo is null");
            return null;
        }

        if (networkInfo.isConnected()) {
            final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            ssid = connectionInfo.getSSID();
            if (connectionInfo != null && ssid != null && !ssid.isEmpty()) {
                return ssid;
            }
        }
        return null;
    }

    public boolean isAdHocNetworkConnected() {
        String ssid = getCurrentSsid(context);
        Log.d(TAG, "Current wifi network is " + ssid);
        if(ssid != null && ssid.contains("Woluxi_"))
            return true;
        return false;
    }

    public void setReconfigureMode(boolean reconfigureMode) {
        isReconfigureMode = reconfigureMode;
    }

}