/*
 * Copyright (C) Woluxi, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ninad Ghodke <ninad@woluxi.com>, 2017.
 */

package com.woluxi.scannerlibdemo;

/**
 * Created by ninadhg on 5/24/17.
 */

import android.app.Activity;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class IPAddressMulticastReceiver extends AsyncTask<String, Integer, String> {
    private static final String TAG = "MCASTrecv";

    final static String INET_ADDR = "224.0.0.1";
    final static int PORT = 1600;
    Activity mActivity;
    String macAddress;

    IPAddressMulticastReceiver(Activity activity, String macAddress){
        Log.d(TAG,"activating multicast class");
        mActivity = activity;
        this.macAddress = macAddress;
    }

    public void waitForData(){
        Log.d(TAG,"checking multicast data");
        InetAddress address = null;
        WifiManager wifiManager = (WifiManager) mActivity.getApplicationContext().getSystemService(mActivity.WIFI_SERVICE);
        int connectTry = 20;
        while(connectTry-- > 0) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            Log.d(TAG,"wifi ssid " + wifiInfo.getSSID() + " state " + ConfigureRemoteDevice.INSTANCE.isConnectedToNetwork());
            if ( wifiInfo.getSSID().contains("Woluxi_") || wifiInfo.getSSID().contains("unknown ssid")
                    ||!ConfigureRemoteDevice.INSTANCE.isConnectedToNetwork()) {
                //Log.d(TAG,"sleeping to check if we are connected");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        try {
            address = InetAddress.getByName(INET_ADDR);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        byte[] buf = new byte[128];
        try (MulticastSocket clientSocket = new MulticastSocket(PORT)){
            int count = 20;
            while (count-- > 0) {
                //Log.d(TAG, " count at " + count);
                try {
                    clientSocket.joinGroup(address);
                    break;
                } catch (SocketException e) {
                    //e.printStackTrace();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                    }
                }
            }
            while (true) {
                DatagramPacket msgPacket = new DatagramPacket(buf, buf.length);
                clientSocket.receive(msgPacket);
                String msg = new String(buf, 0,  msgPacket.getLength()+1);
                JSONObject jObject = new JSONObject(msg);
                String fndMacaddress = jObject.getString("mac_address");
                Log.d(TAG, "received ip address from multicast : " + msg);
                if(fndMacaddress.toLowerCase().contains(macAddress.toLowerCase())) {
                    Intent intent = new Intent("woluxiWifiProvisioningStatus");
                    //// TODO: 6/13/17 check if ipaddress field is present.
                    intent.putExtra("ipaddress", jObject.getString("ip_address"));
                    intent.putExtra("ConfigureState", ConfigureState.CONFIGURATION_IP_ADDRESS_FOUND_STATE);
                    LocalBroadcastManager.getInstance(this.mActivity.getApplicationContext()).sendBroadcast(intent);
                    break;
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        waitForData();
        return null;
    }
}
